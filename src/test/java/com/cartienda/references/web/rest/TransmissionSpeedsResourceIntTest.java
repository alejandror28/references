package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.TransmissionSpeeds;
import com.cartienda.references.repository.TransmissionSpeedsRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TransmissionSpeedsResource REST controller.
 *
 * @see TransmissionSpeedsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class TransmissionSpeedsResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TransmissionSpeedsRepository transmissionSpeedsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTransmissionSpeedsMockMvc;

    private TransmissionSpeeds transmissionSpeeds;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TransmissionSpeedsResource transmissionSpeedsResource = new TransmissionSpeedsResource(transmissionSpeedsRepository);
        this.restTransmissionSpeedsMockMvc = MockMvcBuilders.standaloneSetup(transmissionSpeedsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransmissionSpeeds createEntity(EntityManager em) {
        TransmissionSpeeds transmissionSpeeds = new TransmissionSpeeds()
            .description(DEFAULT_DESCRIPTION);
        return transmissionSpeeds;
    }

    @Before
    public void initTest() {
        transmissionSpeeds = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransmissionSpeeds() throws Exception {
        int databaseSizeBeforeCreate = transmissionSpeedsRepository.findAll().size();

        // Create the TransmissionSpeeds
        restTransmissionSpeedsMockMvc.perform(post("/api/transmission-speeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionSpeeds)))
            .andExpect(status().isCreated());

        // Validate the TransmissionSpeeds in the database
        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeCreate + 1);
        TransmissionSpeeds testTransmissionSpeeds = transmissionSpeedsList.get(transmissionSpeedsList.size() - 1);
        assertThat(testTransmissionSpeeds.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createTransmissionSpeedsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transmissionSpeedsRepository.findAll().size();

        // Create the TransmissionSpeeds with an existing ID
        transmissionSpeeds.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransmissionSpeedsMockMvc.perform(post("/api/transmission-speeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionSpeeds)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = transmissionSpeedsRepository.findAll().size();
        // set the field null
        transmissionSpeeds.setDescription(null);

        // Create the TransmissionSpeeds, which fails.

        restTransmissionSpeedsMockMvc.perform(post("/api/transmission-speeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionSpeeds)))
            .andExpect(status().isBadRequest());

        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTransmissionSpeeds() throws Exception {
        // Initialize the database
        transmissionSpeedsRepository.saveAndFlush(transmissionSpeeds);

        // Get all the transmissionSpeedsList
        restTransmissionSpeedsMockMvc.perform(get("/api/transmission-speeds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transmissionSpeeds.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getTransmissionSpeeds() throws Exception {
        // Initialize the database
        transmissionSpeedsRepository.saveAndFlush(transmissionSpeeds);

        // Get the transmissionSpeeds
        restTransmissionSpeedsMockMvc.perform(get("/api/transmission-speeds/{id}", transmissionSpeeds.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transmissionSpeeds.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransmissionSpeeds() throws Exception {
        // Get the transmissionSpeeds
        restTransmissionSpeedsMockMvc.perform(get("/api/transmission-speeds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransmissionSpeeds() throws Exception {
        // Initialize the database
        transmissionSpeedsRepository.saveAndFlush(transmissionSpeeds);
        int databaseSizeBeforeUpdate = transmissionSpeedsRepository.findAll().size();

        // Update the transmissionSpeeds
        TransmissionSpeeds updatedTransmissionSpeeds = transmissionSpeedsRepository.findOne(transmissionSpeeds.getId());
        updatedTransmissionSpeeds
            .description(UPDATED_DESCRIPTION);

        restTransmissionSpeedsMockMvc.perform(put("/api/transmission-speeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransmissionSpeeds)))
            .andExpect(status().isOk());

        // Validate the TransmissionSpeeds in the database
        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeUpdate);
        TransmissionSpeeds testTransmissionSpeeds = transmissionSpeedsList.get(transmissionSpeedsList.size() - 1);
        assertThat(testTransmissionSpeeds.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTransmissionSpeeds() throws Exception {
        int databaseSizeBeforeUpdate = transmissionSpeedsRepository.findAll().size();

        // Create the TransmissionSpeeds

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTransmissionSpeedsMockMvc.perform(put("/api/transmission-speeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionSpeeds)))
            .andExpect(status().isCreated());

        // Validate the TransmissionSpeeds in the database
        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTransmissionSpeeds() throws Exception {
        // Initialize the database
        transmissionSpeedsRepository.saveAndFlush(transmissionSpeeds);
        int databaseSizeBeforeDelete = transmissionSpeedsRepository.findAll().size();

        // Get the transmissionSpeeds
        restTransmissionSpeedsMockMvc.perform(delete("/api/transmission-speeds/{id}", transmissionSpeeds.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TransmissionSpeeds> transmissionSpeedsList = transmissionSpeedsRepository.findAll();
        assertThat(transmissionSpeedsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransmissionSpeeds.class);
        TransmissionSpeeds transmissionSpeeds1 = new TransmissionSpeeds();
        transmissionSpeeds1.setId(1L);
        TransmissionSpeeds transmissionSpeeds2 = new TransmissionSpeeds();
        transmissionSpeeds2.setId(transmissionSpeeds1.getId());
        assertThat(transmissionSpeeds1).isEqualTo(transmissionSpeeds2);
        transmissionSpeeds2.setId(2L);
        assertThat(transmissionSpeeds1).isNotEqualTo(transmissionSpeeds2);
        transmissionSpeeds1.setId(null);
        assertThat(transmissionSpeeds1).isNotEqualTo(transmissionSpeeds2);
    }
}
