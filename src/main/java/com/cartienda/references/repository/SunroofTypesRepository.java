package com.cartienda.references.repository;

import com.cartienda.references.domain.SunroofTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SunroofTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SunroofTypesRepository extends JpaRepository<SunroofTypes,Long> {
    
}
