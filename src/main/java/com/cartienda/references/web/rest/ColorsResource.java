package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.Colors;

import com.cartienda.references.repository.ColorsRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Colors.
 */
@RestController
@RequestMapping("/api")
public class ColorsResource {

    private final Logger log = LoggerFactory.getLogger(ColorsResource.class);

    private static final String ENTITY_NAME = "colors";

    private final ColorsRepository colorsRepository;

    public ColorsResource(ColorsRepository colorsRepository) {
        this.colorsRepository = colorsRepository;
    }

    /**
     * POST  /colors : Create a new colors.
     *
     * @param colors the colors to create
     * @return the ResponseEntity with status 201 (Created) and with body the new colors, or with status 400 (Bad Request) if the colors has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/colors")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Colors> createColors(@Valid @RequestBody Colors colors) throws URISyntaxException {
        log.debug("REST request to save Colors : {}", colors);
        if (colors.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new colors cannot already have an ID")).body(null);
        }
        Colors result = colorsRepository.save(colors);
        return ResponseEntity.created(new URI("/api/colors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /colors : Updates an existing colors.
     *
     * @param colors the colors to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated colors,
     * or with status 400 (Bad Request) if the colors is not valid,
     * or with status 500 (Internal Server Error) if the colors couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/colors")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Colors> updateColors(@Valid @RequestBody Colors colors) throws URISyntaxException {
        log.debug("REST request to update Colors : {}", colors);
        if (colors.getId() == null) {
            return createColors(colors);
        }
        Colors result = colorsRepository.save(colors);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, colors.getId().toString()))
            .body(result);
    }

    /**
     * GET  /colors : get all the colors.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of colors in body
     */
    @GetMapping("/colors")
    @Timed
    public List<Colors> getAllColors() {
        log.debug("REST request to get all Colors");
        return colorsRepository.findAll();
    }

    /**
     * GET  /colors/:id : get the "id" colors.
     *
     * @param id the id of the colors to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the colors, or with status 404 (Not Found)
     */
    @GetMapping("/colors/{id}")
    @Timed
    public ResponseEntity<Colors> getColors(@PathVariable Long id) {
        log.debug("REST request to get Colors : {}", id);
        Colors colors = colorsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(colors));
    }

    /**
     * DELETE  /colors/:id : delete the "id" colors.
     *
     * @param id the id of the colors to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/colors/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteColors(@PathVariable Long id) {
        log.debug("REST request to delete Colors : {}", id);
        colorsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
