package com.cartienda.references.repository;

import com.cartienda.references.domain.FuelTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the FuelTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FuelTypesRepository extends JpaRepository<FuelTypes,Long> {
    
}
