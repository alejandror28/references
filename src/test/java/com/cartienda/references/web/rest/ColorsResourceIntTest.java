package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.Colors;
import com.cartienda.references.repository.ColorsRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ColorsResource REST controller.
 *
 * @see ColorsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class ColorsResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_HEX_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_HEX_VALUE = "BBBBBBBBBB";

    @Autowired
    private ColorsRepository colorsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restColorsMockMvc;

    private Colors colors;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ColorsResource colorsResource = new ColorsResource(colorsRepository);
        this.restColorsMockMvc = MockMvcBuilders.standaloneSetup(colorsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Colors createEntity(EntityManager em) {
        Colors colors = new Colors()
            .description(DEFAULT_DESCRIPTION)
            .hexValue(DEFAULT_HEX_VALUE);
        return colors;
    }

    @Before
    public void initTest() {
        colors = createEntity(em);
    }

    @Test
    @Transactional
    public void createColors() throws Exception {
        int databaseSizeBeforeCreate = colorsRepository.findAll().size();

        // Create the Colors
        restColorsMockMvc.perform(post("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colors)))
            .andExpect(status().isCreated());

        // Validate the Colors in the database
        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeCreate + 1);
        Colors testColors = colorsList.get(colorsList.size() - 1);
        assertThat(testColors.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testColors.getHexValue()).isEqualTo(DEFAULT_HEX_VALUE);
    }

    @Test
    @Transactional
    public void createColorsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = colorsRepository.findAll().size();

        // Create the Colors with an existing ID
        colors.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restColorsMockMvc.perform(post("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colors)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = colorsRepository.findAll().size();
        // set the field null
        colors.setDescription(null);

        // Create the Colors, which fails.

        restColorsMockMvc.perform(post("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colors)))
            .andExpect(status().isBadRequest());

        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHexValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = colorsRepository.findAll().size();
        // set the field null
        colors.setHexValue(null);

        // Create the Colors, which fails.

        restColorsMockMvc.perform(post("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colors)))
            .andExpect(status().isBadRequest());

        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllColors() throws Exception {
        // Initialize the database
        colorsRepository.saveAndFlush(colors);

        // Get all the colorsList
        restColorsMockMvc.perform(get("/api/colors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(colors.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].hexValue").value(hasItem(DEFAULT_HEX_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getColors() throws Exception {
        // Initialize the database
        colorsRepository.saveAndFlush(colors);

        // Get the colors
        restColorsMockMvc.perform(get("/api/colors/{id}", colors.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(colors.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.hexValue").value(DEFAULT_HEX_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingColors() throws Exception {
        // Get the colors
        restColorsMockMvc.perform(get("/api/colors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateColors() throws Exception {
        // Initialize the database
        colorsRepository.saveAndFlush(colors);
        int databaseSizeBeforeUpdate = colorsRepository.findAll().size();

        // Update the colors
        Colors updatedColors = colorsRepository.findOne(colors.getId());
        updatedColors
            .description(UPDATED_DESCRIPTION)
            .hexValue(UPDATED_HEX_VALUE);

        restColorsMockMvc.perform(put("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedColors)))
            .andExpect(status().isOk());

        // Validate the Colors in the database
        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeUpdate);
        Colors testColors = colorsList.get(colorsList.size() - 1);
        assertThat(testColors.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testColors.getHexValue()).isEqualTo(UPDATED_HEX_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingColors() throws Exception {
        int databaseSizeBeforeUpdate = colorsRepository.findAll().size();

        // Create the Colors

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restColorsMockMvc.perform(put("/api/colors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colors)))
            .andExpect(status().isCreated());

        // Validate the Colors in the database
        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteColors() throws Exception {
        // Initialize the database
        colorsRepository.saveAndFlush(colors);
        int databaseSizeBeforeDelete = colorsRepository.findAll().size();

        // Get the colors
        restColorsMockMvc.perform(delete("/api/colors/{id}", colors.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Colors> colorsList = colorsRepository.findAll();
        assertThat(colorsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Colors.class);
        Colors colors1 = new Colors();
        colors1.setId(1L);
        Colors colors2 = new Colors();
        colors2.setId(colors1.getId());
        assertThat(colors1).isEqualTo(colors2);
        colors2.setId(2L);
        assertThat(colors1).isNotEqualTo(colors2);
        colors1.setId(null);
        assertThat(colors1).isNotEqualTo(colors2);
    }
}
