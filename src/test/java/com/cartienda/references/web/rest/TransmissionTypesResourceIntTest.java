package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.TransmissionTypes;
import com.cartienda.references.repository.TransmissionTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TransmissionTypesResource REST controller.
 *
 * @see TransmissionTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class TransmissionTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TransmissionTypesRepository transmissionTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTransmissionTypesMockMvc;

    private TransmissionTypes transmissionTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TransmissionTypesResource transmissionTypesResource = new TransmissionTypesResource(transmissionTypesRepository);
        this.restTransmissionTypesMockMvc = MockMvcBuilders.standaloneSetup(transmissionTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransmissionTypes createEntity(EntityManager em) {
        TransmissionTypes transmissionTypes = new TransmissionTypes()
            .description(DEFAULT_DESCRIPTION);
        return transmissionTypes;
    }

    @Before
    public void initTest() {
        transmissionTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransmissionTypes() throws Exception {
        int databaseSizeBeforeCreate = transmissionTypesRepository.findAll().size();

        // Create the TransmissionTypes
        restTransmissionTypesMockMvc.perform(post("/api/transmission-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionTypes)))
            .andExpect(status().isCreated());

        // Validate the TransmissionTypes in the database
        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeCreate + 1);
        TransmissionTypes testTransmissionTypes = transmissionTypesList.get(transmissionTypesList.size() - 1);
        assertThat(testTransmissionTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createTransmissionTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transmissionTypesRepository.findAll().size();

        // Create the TransmissionTypes with an existing ID
        transmissionTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransmissionTypesMockMvc.perform(post("/api/transmission-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = transmissionTypesRepository.findAll().size();
        // set the field null
        transmissionTypes.setDescription(null);

        // Create the TransmissionTypes, which fails.

        restTransmissionTypesMockMvc.perform(post("/api/transmission-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionTypes)))
            .andExpect(status().isBadRequest());

        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTransmissionTypes() throws Exception {
        // Initialize the database
        transmissionTypesRepository.saveAndFlush(transmissionTypes);

        // Get all the transmissionTypesList
        restTransmissionTypesMockMvc.perform(get("/api/transmission-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transmissionTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getTransmissionTypes() throws Exception {
        // Initialize the database
        transmissionTypesRepository.saveAndFlush(transmissionTypes);

        // Get the transmissionTypes
        restTransmissionTypesMockMvc.perform(get("/api/transmission-types/{id}", transmissionTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transmissionTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransmissionTypes() throws Exception {
        // Get the transmissionTypes
        restTransmissionTypesMockMvc.perform(get("/api/transmission-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransmissionTypes() throws Exception {
        // Initialize the database
        transmissionTypesRepository.saveAndFlush(transmissionTypes);
        int databaseSizeBeforeUpdate = transmissionTypesRepository.findAll().size();

        // Update the transmissionTypes
        TransmissionTypes updatedTransmissionTypes = transmissionTypesRepository.findOne(transmissionTypes.getId());
        updatedTransmissionTypes
            .description(UPDATED_DESCRIPTION);

        restTransmissionTypesMockMvc.perform(put("/api/transmission-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransmissionTypes)))
            .andExpect(status().isOk());

        // Validate the TransmissionTypes in the database
        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeUpdate);
        TransmissionTypes testTransmissionTypes = transmissionTypesList.get(transmissionTypesList.size() - 1);
        assertThat(testTransmissionTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTransmissionTypes() throws Exception {
        int databaseSizeBeforeUpdate = transmissionTypesRepository.findAll().size();

        // Create the TransmissionTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTransmissionTypesMockMvc.perform(put("/api/transmission-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transmissionTypes)))
            .andExpect(status().isCreated());

        // Validate the TransmissionTypes in the database
        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTransmissionTypes() throws Exception {
        // Initialize the database
        transmissionTypesRepository.saveAndFlush(transmissionTypes);
        int databaseSizeBeforeDelete = transmissionTypesRepository.findAll().size();

        // Get the transmissionTypes
        restTransmissionTypesMockMvc.perform(delete("/api/transmission-types/{id}", transmissionTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TransmissionTypes> transmissionTypesList = transmissionTypesRepository.findAll();
        assertThat(transmissionTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransmissionTypes.class);
        TransmissionTypes transmissionTypes1 = new TransmissionTypes();
        transmissionTypes1.setId(1L);
        TransmissionTypes transmissionTypes2 = new TransmissionTypes();
        transmissionTypes2.setId(transmissionTypes1.getId());
        assertThat(transmissionTypes1).isEqualTo(transmissionTypes2);
        transmissionTypes2.setId(2L);
        assertThat(transmissionTypes1).isNotEqualTo(transmissionTypes2);
        transmissionTypes1.setId(null);
        assertThat(transmissionTypes1).isNotEqualTo(transmissionTypes2);
    }
}
