package com.cartienda.references.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.AirbagTypes;
import com.cartienda.references.security.AuthoritiesConstants;
import com.cartienda.references.repository.AirbagTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.annotation.Secured;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AirbagTypes.
 */
@RestController
@RequestMapping("/api")
public class AirbagTypesResource {

    private final Logger log = LoggerFactory.getLogger(AirbagTypesResource.class);

    private static final String ENTITY_NAME = "airbagTypes";

    private final AirbagTypesRepository airbagTypesRepository;

    public AirbagTypesResource(AirbagTypesRepository airbagTypesRepository) {
        this.airbagTypesRepository = airbagTypesRepository;
    }

    /**
     * POST  /airbag-types : Create a new airbagTypes.
     *
     * @param airbagTypes the airbagTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new airbagTypes, or with status 400 (Bad Request) if the airbagTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/airbag-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirbagTypes> createAirbagTypes(@Valid @RequestBody AirbagTypes airbagTypes) throws URISyntaxException {
        log.debug("REST request to save AirbagTypes : {}", airbagTypes);
        if (airbagTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new airbagTypes cannot already have an ID")).body(null);
        }
        AirbagTypes result = airbagTypesRepository.save(airbagTypes);
        return ResponseEntity.created(new URI("/api/airbag-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /airbag-types : Updates an existing airbagTypes.
     *
     * @param airbagTypes the airbagTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated airbagTypes,
     * or with status 400 (Bad Request) if the airbagTypes is not valid,
     * or with status 500 (Internal Server Error) if the airbagTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/airbag-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirbagTypes> updateAirbagTypes(@Valid @RequestBody AirbagTypes airbagTypes) throws URISyntaxException {
        log.debug("REST request to update AirbagTypes : {}", airbagTypes);
        if (airbagTypes.getId() == null) {
            return createAirbagTypes(airbagTypes);
        }
        AirbagTypes result = airbagTypesRepository.save(airbagTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, airbagTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /airbag-types : get all the airbagTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of airbagTypes in body
     */
    @GetMapping("/airbag-types")
    @Timed
    public List<AirbagTypes> getAllAirbagTypes() {
        log.debug("REST request to get all AirbagTypes");
        return airbagTypesRepository.findAll();
    }

    /**
     * GET  /airbag-types/:id : get the "id" airbagTypes.
     *
     * @param id the id of the airbagTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the airbagTypes, or with status 404 (Not Found)
     */
    @GetMapping("/airbag-types/{id}")
    @Timed
    public ResponseEntity<AirbagTypes> getAirbagTypes(@PathVariable Long id) {
        log.debug("REST request to get AirbagTypes : {}", id);
        AirbagTypes airbagTypes = airbagTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(airbagTypes));
    }

    /**
     * DELETE  /airbag-types/:id : delete the "id" airbagTypes.
     *
     * @param id the id of the airbagTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/airbag-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteAirbagTypes(@PathVariable Long id) {
        log.debug("REST request to delete AirbagTypes : {}", id);
        airbagTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
