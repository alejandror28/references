package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.AirbagTypes;
import com.cartienda.references.repository.AirbagTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AirbagTypesResource REST controller.
 *
 * @see AirbagTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class AirbagTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AirbagTypesRepository airbagTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAirbagTypesMockMvc;

    private AirbagTypes airbagTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AirbagTypesResource airbagTypesResource = new AirbagTypesResource(airbagTypesRepository);
        this.restAirbagTypesMockMvc = MockMvcBuilders.standaloneSetup(airbagTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AirbagTypes createEntity(EntityManager em) {
        AirbagTypes airbagTypes = new AirbagTypes()
            .description(DEFAULT_DESCRIPTION);
        return airbagTypes;
    }

    @Before
    public void initTest() {
        airbagTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createAirbagTypes() throws Exception {
        int databaseSizeBeforeCreate = airbagTypesRepository.findAll().size();

        // Create the AirbagTypes
        restAirbagTypesMockMvc.perform(post("/api/airbag-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airbagTypes)))
            .andExpect(status().isCreated());

        // Validate the AirbagTypes in the database
        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeCreate + 1);
        AirbagTypes testAirbagTypes = airbagTypesList.get(airbagTypesList.size() - 1);
        assertThat(testAirbagTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createAirbagTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = airbagTypesRepository.findAll().size();

        // Create the AirbagTypes with an existing ID
        airbagTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAirbagTypesMockMvc.perform(post("/api/airbag-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airbagTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = airbagTypesRepository.findAll().size();
        // set the field null
        airbagTypes.setDescription(null);

        // Create the AirbagTypes, which fails.

        restAirbagTypesMockMvc.perform(post("/api/airbag-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airbagTypes)))
            .andExpect(status().isBadRequest());

        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAirbagTypes() throws Exception {
        // Initialize the database
        airbagTypesRepository.saveAndFlush(airbagTypes);

        // Get all the airbagTypesList
        restAirbagTypesMockMvc.perform(get("/api/airbag-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(airbagTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getAirbagTypes() throws Exception {
        // Initialize the database
        airbagTypesRepository.saveAndFlush(airbagTypes);

        // Get the airbagTypes
        restAirbagTypesMockMvc.perform(get("/api/airbag-types/{id}", airbagTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(airbagTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAirbagTypes() throws Exception {
        // Get the airbagTypes
        restAirbagTypesMockMvc.perform(get("/api/airbag-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAirbagTypes() throws Exception {
        // Initialize the database
        airbagTypesRepository.saveAndFlush(airbagTypes);
        int databaseSizeBeforeUpdate = airbagTypesRepository.findAll().size();

        // Update the airbagTypes
        AirbagTypes updatedAirbagTypes = airbagTypesRepository.findOne(airbagTypes.getId());
        updatedAirbagTypes
            .description(UPDATED_DESCRIPTION);

        restAirbagTypesMockMvc.perform(put("/api/airbag-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAirbagTypes)))
            .andExpect(status().isOk());

        // Validate the AirbagTypes in the database
        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeUpdate);
        AirbagTypes testAirbagTypes = airbagTypesList.get(airbagTypesList.size() - 1);
        assertThat(testAirbagTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingAirbagTypes() throws Exception {
        int databaseSizeBeforeUpdate = airbagTypesRepository.findAll().size();

        // Create the AirbagTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAirbagTypesMockMvc.perform(put("/api/airbag-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airbagTypes)))
            .andExpect(status().isCreated());

        // Validate the AirbagTypes in the database
        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAirbagTypes() throws Exception {
        // Initialize the database
        airbagTypesRepository.saveAndFlush(airbagTypes);
        int databaseSizeBeforeDelete = airbagTypesRepository.findAll().size();

        // Get the airbagTypes
        restAirbagTypesMockMvc.perform(delete("/api/airbag-types/{id}", airbagTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AirbagTypes> airbagTypesList = airbagTypesRepository.findAll();
        assertThat(airbagTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AirbagTypes.class);
        AirbagTypes airbagTypes1 = new AirbagTypes();
        airbagTypes1.setId(1L);
        AirbagTypes airbagTypes2 = new AirbagTypes();
        airbagTypes2.setId(airbagTypes1.getId());
        assertThat(airbagTypes1).isEqualTo(airbagTypes2);
        airbagTypes2.setId(2L);
        assertThat(airbagTypes1).isNotEqualTo(airbagTypes2);
        airbagTypes1.setId(null);
        assertThat(airbagTypes1).isNotEqualTo(airbagTypes2);
    }
}
