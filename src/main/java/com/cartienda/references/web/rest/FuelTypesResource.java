package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.FuelTypes;

import com.cartienda.references.repository.FuelTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FuelTypes.
 */
@RestController
@RequestMapping("/api")
public class FuelTypesResource {

    private final Logger log = LoggerFactory.getLogger(FuelTypesResource.class);

    private static final String ENTITY_NAME = "fuelTypes";

    private final FuelTypesRepository fuelTypesRepository;

    public FuelTypesResource(FuelTypesRepository fuelTypesRepository) {
        this.fuelTypesRepository = fuelTypesRepository;
    }

    /**
     * POST  /fuel-types : Create a new fuelTypes.
     *
     * @param fuelTypes the fuelTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fuelTypes, or with status 400 (Bad Request) if the fuelTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/fuel-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<FuelTypes> createFuelTypes(@Valid @RequestBody FuelTypes fuelTypes) throws URISyntaxException {
        log.debug("REST request to save FuelTypes : {}", fuelTypes);
        if (fuelTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fuelTypes cannot already have an ID")).body(null);
        }
        FuelTypes result = fuelTypesRepository.save(fuelTypes);
        return ResponseEntity.created(new URI("/api/fuel-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fuel-types : Updates an existing fuelTypes.
     *
     * @param fuelTypes the fuelTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fuelTypes,
     * or with status 400 (Bad Request) if the fuelTypes is not valid,
     * or with status 500 (Internal Server Error) if the fuelTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/fuel-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<FuelTypes> updateFuelTypes(@Valid @RequestBody FuelTypes fuelTypes) throws URISyntaxException {
        log.debug("REST request to update FuelTypes : {}", fuelTypes);
        if (fuelTypes.getId() == null) {
            return createFuelTypes(fuelTypes);
        }
        FuelTypes result = fuelTypesRepository.save(fuelTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fuelTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fuel-types : get all the fuelTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fuelTypes in body
     */
    @GetMapping("/fuel-types")
    @Timed
    public List<FuelTypes> getAllFuelTypes() {
        log.debug("REST request to get all FuelTypes");
        return fuelTypesRepository.findAll();
    }

    /**
     * GET  /fuel-types/:id : get the "id" fuelTypes.
     *
     * @param id the id of the fuelTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fuelTypes, or with status 404 (Not Found)
     */
    @GetMapping("/fuel-types/{id}")
    @Timed
    public ResponseEntity<FuelTypes> getFuelTypes(@PathVariable Long id) {
        log.debug("REST request to get FuelTypes : {}", id);
        FuelTypes fuelTypes = fuelTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fuelTypes));
    }

    /**
     * DELETE  /fuel-types/:id : delete the "id" fuelTypes.
     *
     * @param id the id of the fuelTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fuel-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteFuelTypes(@PathVariable Long id) {
        log.debug("REST request to delete FuelTypes : {}", id);
        fuelTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
