package com.cartienda.references.repository;

import com.cartienda.references.domain.BodyWorkTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BodyWorkTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BodyWorkTypesRepository extends JpaRepository<BodyWorkTypes,Long> {
    
}
