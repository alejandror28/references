package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.AirConditionerControlTypes;
import com.cartienda.references.repository.AirConditionerControlTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AirConditionerControlTypesResource REST controller.
 *
 * @see AirConditionerControlTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class AirConditionerControlTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AirConditionerControlTypesRepository airConditionerControlTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAirConditionerControlTypesMockMvc;

    private AirConditionerControlTypes airConditionerControlTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AirConditionerControlTypesResource airConditionerControlTypesResource = new AirConditionerControlTypesResource(airConditionerControlTypesRepository);
        this.restAirConditionerControlTypesMockMvc = MockMvcBuilders.standaloneSetup(airConditionerControlTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AirConditionerControlTypes createEntity(EntityManager em) {
        AirConditionerControlTypes airConditionerControlTypes = new AirConditionerControlTypes()
            .description(DEFAULT_DESCRIPTION);
        return airConditionerControlTypes;
    }

    @Before
    public void initTest() {
        airConditionerControlTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createAirConditionerControlTypes() throws Exception {
        int databaseSizeBeforeCreate = airConditionerControlTypesRepository.findAll().size();

        // Create the AirConditionerControlTypes
        restAirConditionerControlTypesMockMvc.perform(post("/api/air-conditioner-control-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionerControlTypes)))
            .andExpect(status().isCreated());

        // Validate the AirConditionerControlTypes in the database
        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeCreate + 1);
        AirConditionerControlTypes testAirConditionerControlTypes = airConditionerControlTypesList.get(airConditionerControlTypesList.size() - 1);
        assertThat(testAirConditionerControlTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createAirConditionerControlTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = airConditionerControlTypesRepository.findAll().size();

        // Create the AirConditionerControlTypes with an existing ID
        airConditionerControlTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAirConditionerControlTypesMockMvc.perform(post("/api/air-conditioner-control-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionerControlTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = airConditionerControlTypesRepository.findAll().size();
        // set the field null
        airConditionerControlTypes.setDescription(null);

        // Create the AirConditionerControlTypes, which fails.

        restAirConditionerControlTypesMockMvc.perform(post("/api/air-conditioner-control-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionerControlTypes)))
            .andExpect(status().isBadRequest());

        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAirConditionerControlTypes() throws Exception {
        // Initialize the database
        airConditionerControlTypesRepository.saveAndFlush(airConditionerControlTypes);

        // Get all the airConditionerControlTypesList
        restAirConditionerControlTypesMockMvc.perform(get("/api/air-conditioner-control-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(airConditionerControlTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getAirConditionerControlTypes() throws Exception {
        // Initialize the database
        airConditionerControlTypesRepository.saveAndFlush(airConditionerControlTypes);

        // Get the airConditionerControlTypes
        restAirConditionerControlTypesMockMvc.perform(get("/api/air-conditioner-control-types/{id}", airConditionerControlTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(airConditionerControlTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAirConditionerControlTypes() throws Exception {
        // Get the airConditionerControlTypes
        restAirConditionerControlTypesMockMvc.perform(get("/api/air-conditioner-control-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAirConditionerControlTypes() throws Exception {
        // Initialize the database
        airConditionerControlTypesRepository.saveAndFlush(airConditionerControlTypes);
        int databaseSizeBeforeUpdate = airConditionerControlTypesRepository.findAll().size();

        // Update the airConditionerControlTypes
        AirConditionerControlTypes updatedAirConditionerControlTypes = airConditionerControlTypesRepository.findOne(airConditionerControlTypes.getId());
        updatedAirConditionerControlTypes
            .description(UPDATED_DESCRIPTION);

        restAirConditionerControlTypesMockMvc.perform(put("/api/air-conditioner-control-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAirConditionerControlTypes)))
            .andExpect(status().isOk());

        // Validate the AirConditionerControlTypes in the database
        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeUpdate);
        AirConditionerControlTypes testAirConditionerControlTypes = airConditionerControlTypesList.get(airConditionerControlTypesList.size() - 1);
        assertThat(testAirConditionerControlTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingAirConditionerControlTypes() throws Exception {
        int databaseSizeBeforeUpdate = airConditionerControlTypesRepository.findAll().size();

        // Create the AirConditionerControlTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAirConditionerControlTypesMockMvc.perform(put("/api/air-conditioner-control-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionerControlTypes)))
            .andExpect(status().isCreated());

        // Validate the AirConditionerControlTypes in the database
        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAirConditionerControlTypes() throws Exception {
        // Initialize the database
        airConditionerControlTypesRepository.saveAndFlush(airConditionerControlTypes);
        int databaseSizeBeforeDelete = airConditionerControlTypesRepository.findAll().size();

        // Get the airConditionerControlTypes
        restAirConditionerControlTypesMockMvc.perform(delete("/api/air-conditioner-control-types/{id}", airConditionerControlTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AirConditionerControlTypes> airConditionerControlTypesList = airConditionerControlTypesRepository.findAll();
        assertThat(airConditionerControlTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AirConditionerControlTypes.class);
        AirConditionerControlTypes airConditionerControlTypes1 = new AirConditionerControlTypes();
        airConditionerControlTypes1.setId(1L);
        AirConditionerControlTypes airConditionerControlTypes2 = new AirConditionerControlTypes();
        airConditionerControlTypes2.setId(airConditionerControlTypes1.getId());
        assertThat(airConditionerControlTypes1).isEqualTo(airConditionerControlTypes2);
        airConditionerControlTypes2.setId(2L);
        assertThat(airConditionerControlTypes1).isNotEqualTo(airConditionerControlTypes2);
        airConditionerControlTypes1.setId(null);
        assertThat(airConditionerControlTypes1).isNotEqualTo(airConditionerControlTypes2);
    }
}
