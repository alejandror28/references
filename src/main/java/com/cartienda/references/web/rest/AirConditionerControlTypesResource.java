package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.AirConditionerControlTypes;

import com.cartienda.references.repository.AirConditionerControlTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AirConditionerControlTypes.
 */
@RestController
@RequestMapping("/api")
public class AirConditionerControlTypesResource {

    private final Logger log = LoggerFactory.getLogger(AirConditionerControlTypesResource.class);

    private static final String ENTITY_NAME = "airConditionerControlTypes";

    private final AirConditionerControlTypesRepository airConditionerControlTypesRepository;

    public AirConditionerControlTypesResource(AirConditionerControlTypesRepository airConditionerControlTypesRepository) {
        this.airConditionerControlTypesRepository = airConditionerControlTypesRepository;
    }

    /**
     * POST  /air-conditioner-control-types : Create a new airConditionerControlTypes.
     *
     * @param airConditionerControlTypes the airConditionerControlTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new airConditionerControlTypes, or with status 400 (Bad Request) if the airConditionerControlTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/air-conditioner-control-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirConditionerControlTypes> createAirConditionerControlTypes(@Valid @RequestBody AirConditionerControlTypes airConditionerControlTypes) throws URISyntaxException {
        log.debug("REST request to save AirConditionerControlTypes : {}", airConditionerControlTypes);
        if (airConditionerControlTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new airConditionerControlTypes cannot already have an ID")).body(null);
        }
        AirConditionerControlTypes result = airConditionerControlTypesRepository.save(airConditionerControlTypes);
        return ResponseEntity.created(new URI("/api/air-conditioner-control-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /air-conditioner-control-types : Updates an existing airConditionerControlTypes.
     *
     * @param airConditionerControlTypes the airConditionerControlTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated airConditionerControlTypes,
     * or with status 400 (Bad Request) if the airConditionerControlTypes is not valid,
     * or with status 500 (Internal Server Error) if the airConditionerControlTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/air-conditioner-control-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirConditionerControlTypes> updateAirConditionerControlTypes(@Valid @RequestBody AirConditionerControlTypes airConditionerControlTypes) throws URISyntaxException {
        log.debug("REST request to update AirConditionerControlTypes : {}", airConditionerControlTypes);
        if (airConditionerControlTypes.getId() == null) {
            return createAirConditionerControlTypes(airConditionerControlTypes);
        }
        AirConditionerControlTypes result = airConditionerControlTypesRepository.save(airConditionerControlTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, airConditionerControlTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /air-conditioner-control-types : get all the airConditionerControlTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of airConditionerControlTypes in body
     */
    @GetMapping("/air-conditioner-control-types")
    @Timed
    public List<AirConditionerControlTypes> getAllAirConditionerControlTypes() {
        log.debug("REST request to get all AirConditionerControlTypes");
        return airConditionerControlTypesRepository.findAll();
    }

    /**
     * GET  /air-conditioner-control-types/:id : get the "id" airConditionerControlTypes.
     *
     * @param id the id of the airConditionerControlTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the airConditionerControlTypes, or with status 404 (Not Found)
     */
    @GetMapping("/air-conditioner-control-types/{id}")
    @Timed
    public ResponseEntity<AirConditionerControlTypes> getAirConditionerControlTypes(@PathVariable Long id) {
        log.debug("REST request to get AirConditionerControlTypes : {}", id);
        AirConditionerControlTypes airConditionerControlTypes = airConditionerControlTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(airConditionerControlTypes));
    }

    /**
     * DELETE  /air-conditioner-control-types/:id : delete the "id" airConditionerControlTypes.
     *
     * @param id the id of the airConditionerControlTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/air-conditioner-control-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteAirConditionerControlTypes(@PathVariable Long id) {
        log.debug("REST request to delete AirConditionerControlTypes : {}", id);
        airConditionerControlTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
