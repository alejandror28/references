package com.cartienda.references.repository;

import com.cartienda.references.domain.OwnerDocumentTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the OwnerDocumentTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OwnerDocumentTypesRepository extends JpaRepository<OwnerDocumentTypes,Long> {
    
}
