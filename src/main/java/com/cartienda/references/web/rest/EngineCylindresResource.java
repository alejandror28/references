package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.EngineCylindres;

import com.cartienda.references.repository.EngineCylindresRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EngineCylindres.
 */
@RestController
@RequestMapping("/api")
public class EngineCylindresResource {

    private final Logger log = LoggerFactory.getLogger(EngineCylindresResource.class);

    private static final String ENTITY_NAME = "engineCylindres";

    private final EngineCylindresRepository engineCylindresRepository;

    public EngineCylindresResource(EngineCylindresRepository engineCylindresRepository) {
        this.engineCylindresRepository = engineCylindresRepository;
    }

    /**
     * POST  /engine-cylindres : Create a new engineCylindres.
     *
     * @param engineCylindres the engineCylindres to create
     * @return the ResponseEntity with status 201 (Created) and with body the new engineCylindres, or with status 400 (Bad Request) if the engineCylindres has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/engine-cylindres")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<EngineCylindres> createEngineCylindres(@Valid @RequestBody EngineCylindres engineCylindres) throws URISyntaxException {
        log.debug("REST request to save EngineCylindres : {}", engineCylindres);
        if (engineCylindres.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new engineCylindres cannot already have an ID")).body(null);
        }
        EngineCylindres result = engineCylindresRepository.save(engineCylindres);
        return ResponseEntity.created(new URI("/api/engine-cylindres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /engine-cylindres : Updates an existing engineCylindres.
     *
     * @param engineCylindres the engineCylindres to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated engineCylindres,
     * or with status 400 (Bad Request) if the engineCylindres is not valid,
     * or with status 500 (Internal Server Error) if the engineCylindres couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/engine-cylindres")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<EngineCylindres> updateEngineCylindres(@Valid @RequestBody EngineCylindres engineCylindres) throws URISyntaxException {
        log.debug("REST request to update EngineCylindres : {}", engineCylindres);
        if (engineCylindres.getId() == null) {
            return createEngineCylindres(engineCylindres);
        }
        EngineCylindres result = engineCylindresRepository.save(engineCylindres);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, engineCylindres.getId().toString()))
            .body(result);
    }

    /**
     * GET  /engine-cylindres : get all the engineCylindres.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of engineCylindres in body
     */
    @GetMapping("/engine-cylindres")
    @Timed
    public List<EngineCylindres> getAllEngineCylindres() {
        log.debug("REST request to get all EngineCylindres");
        return engineCylindresRepository.findAll();
    }

    /**
     * GET  /engine-cylindres/:id : get the "id" engineCylindres.
     *
     * @param id the id of the engineCylindres to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the engineCylindres, or with status 404 (Not Found)
     */
    @GetMapping("/engine-cylindres/{id}")
    @Timed
    public ResponseEntity<EngineCylindres> getEngineCylindres(@PathVariable Long id) {
        log.debug("REST request to get EngineCylindres : {}", id);
        EngineCylindres engineCylindres = engineCylindresRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(engineCylindres));
    }

    /**
     * DELETE  /engine-cylindres/:id : delete the "id" engineCylindres.
     *
     * @param id the id of the engineCylindres to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/engine-cylindres/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteEngineCylindres(@PathVariable Long id) {
        log.debug("REST request to delete EngineCylindres : {}", id);
        engineCylindresRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
