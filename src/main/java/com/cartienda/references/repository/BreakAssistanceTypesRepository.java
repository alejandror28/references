package com.cartienda.references.repository;

import com.cartienda.references.domain.BreakAssistanceTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BreakAssistanceTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BreakAssistanceTypesRepository extends JpaRepository<BreakAssistanceTypes,Long> {
    
}
