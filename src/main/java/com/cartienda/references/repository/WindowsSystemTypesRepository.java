package com.cartienda.references.repository;

import com.cartienda.references.domain.WindowsSystemTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WindowsSystemTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WindowsSystemTypesRepository extends JpaRepository<WindowsSystemTypes,Long> {
    
}
