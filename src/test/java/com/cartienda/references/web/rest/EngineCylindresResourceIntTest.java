package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.EngineCylindres;
import com.cartienda.references.repository.EngineCylindresRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EngineCylindresResource REST controller.
 *
 * @see EngineCylindresResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class EngineCylindresResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private EngineCylindresRepository engineCylindresRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEngineCylindresMockMvc;

    private EngineCylindres engineCylindres;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EngineCylindresResource engineCylindresResource = new EngineCylindresResource(engineCylindresRepository);
        this.restEngineCylindresMockMvc = MockMvcBuilders.standaloneSetup(engineCylindresResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EngineCylindres createEntity(EntityManager em) {
        EngineCylindres engineCylindres = new EngineCylindres()
            .description(DEFAULT_DESCRIPTION);
        return engineCylindres;
    }

    @Before
    public void initTest() {
        engineCylindres = createEntity(em);
    }

    @Test
    @Transactional
    public void createEngineCylindres() throws Exception {
        int databaseSizeBeforeCreate = engineCylindresRepository.findAll().size();

        // Create the EngineCylindres
        restEngineCylindresMockMvc.perform(post("/api/engine-cylindres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(engineCylindres)))
            .andExpect(status().isCreated());

        // Validate the EngineCylindres in the database
        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeCreate + 1);
        EngineCylindres testEngineCylindres = engineCylindresList.get(engineCylindresList.size() - 1);
        assertThat(testEngineCylindres.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createEngineCylindresWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = engineCylindresRepository.findAll().size();

        // Create the EngineCylindres with an existing ID
        engineCylindres.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEngineCylindresMockMvc.perform(post("/api/engine-cylindres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(engineCylindres)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = engineCylindresRepository.findAll().size();
        // set the field null
        engineCylindres.setDescription(null);

        // Create the EngineCylindres, which fails.

        restEngineCylindresMockMvc.perform(post("/api/engine-cylindres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(engineCylindres)))
            .andExpect(status().isBadRequest());

        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEngineCylindres() throws Exception {
        // Initialize the database
        engineCylindresRepository.saveAndFlush(engineCylindres);

        // Get all the engineCylindresList
        restEngineCylindresMockMvc.perform(get("/api/engine-cylindres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(engineCylindres.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getEngineCylindres() throws Exception {
        // Initialize the database
        engineCylindresRepository.saveAndFlush(engineCylindres);

        // Get the engineCylindres
        restEngineCylindresMockMvc.perform(get("/api/engine-cylindres/{id}", engineCylindres.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(engineCylindres.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEngineCylindres() throws Exception {
        // Get the engineCylindres
        restEngineCylindresMockMvc.perform(get("/api/engine-cylindres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEngineCylindres() throws Exception {
        // Initialize the database
        engineCylindresRepository.saveAndFlush(engineCylindres);
        int databaseSizeBeforeUpdate = engineCylindresRepository.findAll().size();

        // Update the engineCylindres
        EngineCylindres updatedEngineCylindres = engineCylindresRepository.findOne(engineCylindres.getId());
        updatedEngineCylindres
            .description(UPDATED_DESCRIPTION);

        restEngineCylindresMockMvc.perform(put("/api/engine-cylindres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEngineCylindres)))
            .andExpect(status().isOk());

        // Validate the EngineCylindres in the database
        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeUpdate);
        EngineCylindres testEngineCylindres = engineCylindresList.get(engineCylindresList.size() - 1);
        assertThat(testEngineCylindres.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingEngineCylindres() throws Exception {
        int databaseSizeBeforeUpdate = engineCylindresRepository.findAll().size();

        // Create the EngineCylindres

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEngineCylindresMockMvc.perform(put("/api/engine-cylindres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(engineCylindres)))
            .andExpect(status().isCreated());

        // Validate the EngineCylindres in the database
        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEngineCylindres() throws Exception {
        // Initialize the database
        engineCylindresRepository.saveAndFlush(engineCylindres);
        int databaseSizeBeforeDelete = engineCylindresRepository.findAll().size();

        // Get the engineCylindres
        restEngineCylindresMockMvc.perform(delete("/api/engine-cylindres/{id}", engineCylindres.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EngineCylindres> engineCylindresList = engineCylindresRepository.findAll();
        assertThat(engineCylindresList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EngineCylindres.class);
        EngineCylindres engineCylindres1 = new EngineCylindres();
        engineCylindres1.setId(1L);
        EngineCylindres engineCylindres2 = new EngineCylindres();
        engineCylindres2.setId(engineCylindres1.getId());
        assertThat(engineCylindres1).isEqualTo(engineCylindres2);
        engineCylindres2.setId(2L);
        assertThat(engineCylindres1).isNotEqualTo(engineCylindres2);
        engineCylindres1.setId(null);
        assertThat(engineCylindres1).isNotEqualTo(engineCylindres2);
    }
}
