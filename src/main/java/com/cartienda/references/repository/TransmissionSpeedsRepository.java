package com.cartienda.references.repository;

import com.cartienda.references.domain.TransmissionSpeeds;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TransmissionSpeeds entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransmissionSpeedsRepository extends JpaRepository<TransmissionSpeeds,Long> {
    
}
