package com.cartienda.references.repository;

import com.cartienda.references.domain.ServiceTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceTypesRepository extends JpaRepository<ServiceTypes,Long> {
    
}
