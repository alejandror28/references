package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.PlatePublicationTypes;
import com.cartienda.references.repository.PlatePublicationTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlatePublicationTypesResource REST controller.
 *
 * @see PlatePublicationTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class PlatePublicationTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private PlatePublicationTypesRepository platePublicationTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlatePublicationTypesMockMvc;

    private PlatePublicationTypes platePublicationTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlatePublicationTypesResource platePublicationTypesResource = new PlatePublicationTypesResource(platePublicationTypesRepository);
        this.restPlatePublicationTypesMockMvc = MockMvcBuilders.standaloneSetup(platePublicationTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlatePublicationTypes createEntity(EntityManager em) {
        PlatePublicationTypes platePublicationTypes = new PlatePublicationTypes()
            .description(DEFAULT_DESCRIPTION);
        return platePublicationTypes;
    }

    @Before
    public void initTest() {
        platePublicationTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlatePublicationTypes() throws Exception {
        int databaseSizeBeforeCreate = platePublicationTypesRepository.findAll().size();

        // Create the PlatePublicationTypes
        restPlatePublicationTypesMockMvc.perform(post("/api/plate-publication-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(platePublicationTypes)))
            .andExpect(status().isCreated());

        // Validate the PlatePublicationTypes in the database
        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeCreate + 1);
        PlatePublicationTypes testPlatePublicationTypes = platePublicationTypesList.get(platePublicationTypesList.size() - 1);
        assertThat(testPlatePublicationTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPlatePublicationTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = platePublicationTypesRepository.findAll().size();

        // Create the PlatePublicationTypes with an existing ID
        platePublicationTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlatePublicationTypesMockMvc.perform(post("/api/plate-publication-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(platePublicationTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = platePublicationTypesRepository.findAll().size();
        // set the field null
        platePublicationTypes.setDescription(null);

        // Create the PlatePublicationTypes, which fails.

        restPlatePublicationTypesMockMvc.perform(post("/api/plate-publication-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(platePublicationTypes)))
            .andExpect(status().isBadRequest());

        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlatePublicationTypes() throws Exception {
        // Initialize the database
        platePublicationTypesRepository.saveAndFlush(platePublicationTypes);

        // Get all the platePublicationTypesList
        restPlatePublicationTypesMockMvc.perform(get("/api/plate-publication-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(platePublicationTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPlatePublicationTypes() throws Exception {
        // Initialize the database
        platePublicationTypesRepository.saveAndFlush(platePublicationTypes);

        // Get the platePublicationTypes
        restPlatePublicationTypesMockMvc.perform(get("/api/plate-publication-types/{id}", platePublicationTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(platePublicationTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlatePublicationTypes() throws Exception {
        // Get the platePublicationTypes
        restPlatePublicationTypesMockMvc.perform(get("/api/plate-publication-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlatePublicationTypes() throws Exception {
        // Initialize the database
        platePublicationTypesRepository.saveAndFlush(platePublicationTypes);
        int databaseSizeBeforeUpdate = platePublicationTypesRepository.findAll().size();

        // Update the platePublicationTypes
        PlatePublicationTypes updatedPlatePublicationTypes = platePublicationTypesRepository.findOne(platePublicationTypes.getId());
        updatedPlatePublicationTypes
            .description(UPDATED_DESCRIPTION);

        restPlatePublicationTypesMockMvc.perform(put("/api/plate-publication-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlatePublicationTypes)))
            .andExpect(status().isOk());

        // Validate the PlatePublicationTypes in the database
        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeUpdate);
        PlatePublicationTypes testPlatePublicationTypes = platePublicationTypesList.get(platePublicationTypesList.size() - 1);
        assertThat(testPlatePublicationTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingPlatePublicationTypes() throws Exception {
        int databaseSizeBeforeUpdate = platePublicationTypesRepository.findAll().size();

        // Create the PlatePublicationTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlatePublicationTypesMockMvc.perform(put("/api/plate-publication-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(platePublicationTypes)))
            .andExpect(status().isCreated());

        // Validate the PlatePublicationTypes in the database
        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlatePublicationTypes() throws Exception {
        // Initialize the database
        platePublicationTypesRepository.saveAndFlush(platePublicationTypes);
        int databaseSizeBeforeDelete = platePublicationTypesRepository.findAll().size();

        // Get the platePublicationTypes
        restPlatePublicationTypesMockMvc.perform(delete("/api/plate-publication-types/{id}", platePublicationTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlatePublicationTypes> platePublicationTypesList = platePublicationTypesRepository.findAll();
        assertThat(platePublicationTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlatePublicationTypes.class);
        PlatePublicationTypes platePublicationTypes1 = new PlatePublicationTypes();
        platePublicationTypes1.setId(1L);
        PlatePublicationTypes platePublicationTypes2 = new PlatePublicationTypes();
        platePublicationTypes2.setId(platePublicationTypes1.getId());
        assertThat(platePublicationTypes1).isEqualTo(platePublicationTypes2);
        platePublicationTypes2.setId(2L);
        assertThat(platePublicationTypes1).isNotEqualTo(platePublicationTypes2);
        platePublicationTypes1.setId(null);
        assertThat(platePublicationTypes1).isNotEqualTo(platePublicationTypes2);
    }
}
