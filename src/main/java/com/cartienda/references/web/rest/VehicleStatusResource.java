package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.VehicleStatus;

import com.cartienda.references.repository.VehicleStatusRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing VehicleStatus.
 */
@RestController
@RequestMapping("/api")
public class VehicleStatusResource {

    private final Logger log = LoggerFactory.getLogger(VehicleStatusResource.class);

    private static final String ENTITY_NAME = "vehicleStatus";

    private final VehicleStatusRepository vehicleStatusRepository;

    public VehicleStatusResource(VehicleStatusRepository vehicleStatusRepository) {
        this.vehicleStatusRepository = vehicleStatusRepository;
    }

    /**
     * POST  /vehicle-statuses : Create a new vehicleStatus.
     *
     * @param vehicleStatus the vehicleStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleStatus, or with status 400 (Bad Request) if the vehicleStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-statuses")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<VehicleStatus> createVehicleStatus(@Valid @RequestBody VehicleStatus vehicleStatus) throws URISyntaxException {
        log.debug("REST request to save VehicleStatus : {}", vehicleStatus);
        if (vehicleStatus.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleStatus cannot already have an ID")).body(null);
        }
        VehicleStatus result = vehicleStatusRepository.save(vehicleStatus);
        return ResponseEntity.created(new URI("/api/vehicle-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vehicle-statuses : Updates an existing vehicleStatus.
     *
     * @param vehicleStatus the vehicleStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleStatus,
     * or with status 400 (Bad Request) if the vehicleStatus is not valid,
     * or with status 500 (Internal Server Error) if the vehicleStatus couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-statuses")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<VehicleStatus> updateVehicleStatus(@Valid @RequestBody VehicleStatus vehicleStatus) throws URISyntaxException {
        log.debug("REST request to update VehicleStatus : {}", vehicleStatus);
        if (vehicleStatus.getId() == null) {
            return createVehicleStatus(vehicleStatus);
        }
        VehicleStatus result = vehicleStatusRepository.save(vehicleStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-statuses : get all the vehicleStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleStatuses in body
     */
    @GetMapping("/vehicle-statuses")
    @Timed
    public List<VehicleStatus> getAllVehicleStatuses() {
        log.debug("REST request to get all VehicleStatuses");
        return vehicleStatusRepository.findAll();
    }

    /**
     * GET  /vehicle-statuses/:id : get the "id" vehicleStatus.
     *
     * @param id the id of the vehicleStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleStatus, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-statuses/{id}")
    @Timed
    public ResponseEntity<VehicleStatus> getVehicleStatus(@PathVariable Long id) {
        log.debug("REST request to get VehicleStatus : {}", id);
        VehicleStatus vehicleStatus = vehicleStatusRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleStatus));
    }

    /**
     * DELETE  /vehicle-statuses/:id : delete the "id" vehicleStatus.
     *
     * @param id the id of the vehicleStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-statuses/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteVehicleStatus(@PathVariable Long id) {
        log.debug("REST request to delete VehicleStatus : {}", id);
        vehicleStatusRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
