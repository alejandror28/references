package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.TractionTypes;
import com.cartienda.references.repository.TractionTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TractionTypesResource REST controller.
 *
 * @see TractionTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class TractionTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TractionTypesRepository tractionTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTractionTypesMockMvc;

    private TractionTypes tractionTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TractionTypesResource tractionTypesResource = new TractionTypesResource(tractionTypesRepository);
        this.restTractionTypesMockMvc = MockMvcBuilders.standaloneSetup(tractionTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TractionTypes createEntity(EntityManager em) {
        TractionTypes tractionTypes = new TractionTypes()
            .description(DEFAULT_DESCRIPTION);
        return tractionTypes;
    }

    @Before
    public void initTest() {
        tractionTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createTractionTypes() throws Exception {
        int databaseSizeBeforeCreate = tractionTypesRepository.findAll().size();

        // Create the TractionTypes
        restTractionTypesMockMvc.perform(post("/api/traction-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tractionTypes)))
            .andExpect(status().isCreated());

        // Validate the TractionTypes in the database
        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeCreate + 1);
        TractionTypes testTractionTypes = tractionTypesList.get(tractionTypesList.size() - 1);
        assertThat(testTractionTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createTractionTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tractionTypesRepository.findAll().size();

        // Create the TractionTypes with an existing ID
        tractionTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTractionTypesMockMvc.perform(post("/api/traction-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tractionTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = tractionTypesRepository.findAll().size();
        // set the field null
        tractionTypes.setDescription(null);

        // Create the TractionTypes, which fails.

        restTractionTypesMockMvc.perform(post("/api/traction-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tractionTypes)))
            .andExpect(status().isBadRequest());

        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTractionTypes() throws Exception {
        // Initialize the database
        tractionTypesRepository.saveAndFlush(tractionTypes);

        // Get all the tractionTypesList
        restTractionTypesMockMvc.perform(get("/api/traction-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tractionTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getTractionTypes() throws Exception {
        // Initialize the database
        tractionTypesRepository.saveAndFlush(tractionTypes);

        // Get the tractionTypes
        restTractionTypesMockMvc.perform(get("/api/traction-types/{id}", tractionTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tractionTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTractionTypes() throws Exception {
        // Get the tractionTypes
        restTractionTypesMockMvc.perform(get("/api/traction-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTractionTypes() throws Exception {
        // Initialize the database
        tractionTypesRepository.saveAndFlush(tractionTypes);
        int databaseSizeBeforeUpdate = tractionTypesRepository.findAll().size();

        // Update the tractionTypes
        TractionTypes updatedTractionTypes = tractionTypesRepository.findOne(tractionTypes.getId());
        updatedTractionTypes
            .description(UPDATED_DESCRIPTION);

        restTractionTypesMockMvc.perform(put("/api/traction-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTractionTypes)))
            .andExpect(status().isOk());

        // Validate the TractionTypes in the database
        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeUpdate);
        TractionTypes testTractionTypes = tractionTypesList.get(tractionTypesList.size() - 1);
        assertThat(testTractionTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTractionTypes() throws Exception {
        int databaseSizeBeforeUpdate = tractionTypesRepository.findAll().size();

        // Create the TractionTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTractionTypesMockMvc.perform(put("/api/traction-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tractionTypes)))
            .andExpect(status().isCreated());

        // Validate the TractionTypes in the database
        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTractionTypes() throws Exception {
        // Initialize the database
        tractionTypesRepository.saveAndFlush(tractionTypes);
        int databaseSizeBeforeDelete = tractionTypesRepository.findAll().size();

        // Get the tractionTypes
        restTractionTypesMockMvc.perform(delete("/api/traction-types/{id}", tractionTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TractionTypes> tractionTypesList = tractionTypesRepository.findAll();
        assertThat(tractionTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TractionTypes.class);
        TractionTypes tractionTypes1 = new TractionTypes();
        tractionTypes1.setId(1L);
        TractionTypes tractionTypes2 = new TractionTypes();
        tractionTypes2.setId(tractionTypes1.getId());
        assertThat(tractionTypes1).isEqualTo(tractionTypes2);
        tractionTypes2.setId(2L);
        assertThat(tractionTypes1).isNotEqualTo(tractionTypes2);
        tractionTypes1.setId(null);
        assertThat(tractionTypes1).isNotEqualTo(tractionTypes2);
    }
}
