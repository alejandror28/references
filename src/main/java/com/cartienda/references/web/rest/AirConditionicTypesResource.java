package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.AirConditionicTypes;

import com.cartienda.references.repository.AirConditionicTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AirConditionicTypes.
 */
@RestController
@RequestMapping("/api")
public class AirConditionicTypesResource {

    private final Logger log = LoggerFactory.getLogger(AirConditionicTypesResource.class);

    private static final String ENTITY_NAME = "airConditionicTypes";

    private final AirConditionicTypesRepository airConditionicTypesRepository;

    public AirConditionicTypesResource(AirConditionicTypesRepository airConditionicTypesRepository) {
        this.airConditionicTypesRepository = airConditionicTypesRepository;
    }

    /**
     * POST  /air-conditionic-types : Create a new airConditionicTypes.
     *
     * @param airConditionicTypes the airConditionicTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new airConditionicTypes, or with status 400 (Bad Request) if the airConditionicTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/air-conditionic-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirConditionicTypes> createAirConditionicTypes(@Valid @RequestBody AirConditionicTypes airConditionicTypes) throws URISyntaxException {
        log.debug("REST request to save AirConditionicTypes : {}", airConditionicTypes);
        if (airConditionicTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new airConditionicTypes cannot already have an ID")).body(null);
        }
        AirConditionicTypes result = airConditionicTypesRepository.save(airConditionicTypes);
        return ResponseEntity.created(new URI("/api/air-conditionic-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /air-conditionic-types : Updates an existing airConditionicTypes.
     *
     * @param airConditionicTypes the airConditionicTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated airConditionicTypes,
     * or with status 400 (Bad Request) if the airConditionicTypes is not valid,
     * or with status 500 (Internal Server Error) if the airConditionicTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/air-conditionic-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<AirConditionicTypes> updateAirConditionicTypes(@Valid @RequestBody AirConditionicTypes airConditionicTypes) throws URISyntaxException {
        log.debug("REST request to update AirConditionicTypes : {}", airConditionicTypes);
        if (airConditionicTypes.getId() == null) {
            return createAirConditionicTypes(airConditionicTypes);
        }
        AirConditionicTypes result = airConditionicTypesRepository.save(airConditionicTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, airConditionicTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /air-conditionic-types : get all the airConditionicTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of airConditionicTypes in body
     */
    @GetMapping("/air-conditionic-types")
    @Timed
    public List<AirConditionicTypes> getAllAirConditionicTypes() {
        log.debug("REST request to get all AirConditionicTypes");
        return airConditionicTypesRepository.findAll();
    }

    /**
     * GET  /air-conditionic-types/:id : get the "id" airConditionicTypes.
     *
     * @param id the id of the airConditionicTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the airConditionicTypes, or with status 404 (Not Found)
     */
    @GetMapping("/air-conditionic-types/{id}")
    @Timed
    public ResponseEntity<AirConditionicTypes> getAirConditionicTypes(@PathVariable Long id) {
        log.debug("REST request to get AirConditionicTypes : {}", id);
        AirConditionicTypes airConditionicTypes = airConditionicTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(airConditionicTypes));
    }

    /**
     * DELETE  /air-conditionic-types/:id : delete the "id" airConditionicTypes.
     *
     * @param id the id of the airConditionicTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/air-conditionic-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteAirConditionicTypes(@PathVariable Long id) {
        log.debug("REST request to delete AirConditionicTypes : {}", id);
        airConditionicTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
