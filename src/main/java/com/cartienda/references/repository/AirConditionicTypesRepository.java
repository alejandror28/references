package com.cartienda.references.repository;

import com.cartienda.references.domain.AirConditionicTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AirConditionicTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AirConditionicTypesRepository extends JpaRepository<AirConditionicTypes,Long> {
    
}
