package com.cartienda.references.repository;

import com.cartienda.references.domain.PlatePublicationTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PlatePublicationTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlatePublicationTypesRepository extends JpaRepository<PlatePublicationTypes,Long> {
    
}
