package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.UpholsteryTypes;

import com.cartienda.references.repository.UpholsteryTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UpholsteryTypes.
 */
@RestController
@RequestMapping("/api")
public class UpholsteryTypesResource {

    private final Logger log = LoggerFactory.getLogger(UpholsteryTypesResource.class);

    private static final String ENTITY_NAME = "upholsteryTypes";

    private final UpholsteryTypesRepository upholsteryTypesRepository;

    public UpholsteryTypesResource(UpholsteryTypesRepository upholsteryTypesRepository) {
        this.upholsteryTypesRepository = upholsteryTypesRepository;
    }

    /**
     * POST  /upholstery-types : Create a new upholsteryTypes.
     *
     * @param upholsteryTypes the upholsteryTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new upholsteryTypes, or with status 400 (Bad Request) if the upholsteryTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/upholstery-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<UpholsteryTypes> createUpholsteryTypes(@Valid @RequestBody UpholsteryTypes upholsteryTypes) throws URISyntaxException {
        log.debug("REST request to save UpholsteryTypes : {}", upholsteryTypes);
        if (upholsteryTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new upholsteryTypes cannot already have an ID")).body(null);
        }
        UpholsteryTypes result = upholsteryTypesRepository.save(upholsteryTypes);
        return ResponseEntity.created(new URI("/api/upholstery-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /upholstery-types : Updates an existing upholsteryTypes.
     *
     * @param upholsteryTypes the upholsteryTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated upholsteryTypes,
     * or with status 400 (Bad Request) if the upholsteryTypes is not valid,
     * or with status 500 (Internal Server Error) if the upholsteryTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/upholstery-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<UpholsteryTypes> updateUpholsteryTypes(@Valid @RequestBody UpholsteryTypes upholsteryTypes) throws URISyntaxException {
        log.debug("REST request to update UpholsteryTypes : {}", upholsteryTypes);
        if (upholsteryTypes.getId() == null) {
            return createUpholsteryTypes(upholsteryTypes);
        }
        UpholsteryTypes result = upholsteryTypesRepository.save(upholsteryTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, upholsteryTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /upholstery-types : get all the upholsteryTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of upholsteryTypes in body
     */
    @GetMapping("/upholstery-types")
    @Timed
    public List<UpholsteryTypes> getAllUpholsteryTypes() {
        log.debug("REST request to get all UpholsteryTypes");
        return upholsteryTypesRepository.findAll();
    }

    /**
     * GET  /upholstery-types/:id : get the "id" upholsteryTypes.
     *
     * @param id the id of the upholsteryTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the upholsteryTypes, or with status 404 (Not Found)
     */
    @GetMapping("/upholstery-types/{id}")
    @Timed
    public ResponseEntity<UpholsteryTypes> getUpholsteryTypes(@PathVariable Long id) {
        log.debug("REST request to get UpholsteryTypes : {}", id);
        UpholsteryTypes upholsteryTypes = upholsteryTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(upholsteryTypes));
    }

    /**
     * DELETE  /upholstery-types/:id : delete the "id" upholsteryTypes.
     *
     * @param id the id of the upholsteryTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/upholstery-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteUpholsteryTypes(@PathVariable Long id) {
        log.debug("REST request to delete UpholsteryTypes : {}", id);
        upholsteryTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
