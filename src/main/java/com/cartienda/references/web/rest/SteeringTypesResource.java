package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.SteeringTypes;

import com.cartienda.references.repository.SteeringTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SteeringTypes.
 */
@RestController
@RequestMapping("/api")
public class SteeringTypesResource {

    private final Logger log = LoggerFactory.getLogger(SteeringTypesResource.class);

    private static final String ENTITY_NAME = "steeringTypes";

    private final SteeringTypesRepository steeringTypesRepository;

    public SteeringTypesResource(SteeringTypesRepository steeringTypesRepository) {
        this.steeringTypesRepository = steeringTypesRepository;
    }

    /**
     * POST  /steering-types : Create a new steeringTypes.
     *
     * @param steeringTypes the steeringTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new steeringTypes, or with status 400 (Bad Request) if the steeringTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/steering-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<SteeringTypes> createSteeringTypes(@Valid @RequestBody SteeringTypes steeringTypes) throws URISyntaxException {
        log.debug("REST request to save SteeringTypes : {}", steeringTypes);
        if (steeringTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new steeringTypes cannot already have an ID")).body(null);
        }
        SteeringTypes result = steeringTypesRepository.save(steeringTypes);
        return ResponseEntity.created(new URI("/api/steering-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /steering-types : Updates an existing steeringTypes.
     *
     * @param steeringTypes the steeringTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated steeringTypes,
     * or with status 400 (Bad Request) if the steeringTypes is not valid,
     * or with status 500 (Internal Server Error) if the steeringTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/steering-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<SteeringTypes> updateSteeringTypes(@Valid @RequestBody SteeringTypes steeringTypes) throws URISyntaxException {
        log.debug("REST request to update SteeringTypes : {}", steeringTypes);
        if (steeringTypes.getId() == null) {
            return createSteeringTypes(steeringTypes);
        }
        SteeringTypes result = steeringTypesRepository.save(steeringTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, steeringTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /steering-types : get all the steeringTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of steeringTypes in body
     */
    @GetMapping("/steering-types")
    @Timed
    public List<SteeringTypes> getAllSteeringTypes() {
        log.debug("REST request to get all SteeringTypes");
        return steeringTypesRepository.findAll();
    }

    /**
     * GET  /steering-types/:id : get the "id" steeringTypes.
     *
     * @param id the id of the steeringTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the steeringTypes, or with status 404 (Not Found)
     */
    @GetMapping("/steering-types/{id}")
    @Timed
    public ResponseEntity<SteeringTypes> getSteeringTypes(@PathVariable Long id) {
        log.debug("REST request to get SteeringTypes : {}", id);
        SteeringTypes steeringTypes = steeringTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(steeringTypes));
    }

    /**
     * DELETE  /steering-types/:id : delete the "id" steeringTypes.
     *
     * @param id the id of the steeringTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/steering-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteSteeringTypes(@PathVariable Long id) {
        log.debug("REST request to delete SteeringTypes : {}", id);
        steeringTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
