package com.cartienda.references.repository;

import com.cartienda.references.domain.TractionTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TractionTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TractionTypesRepository extends JpaRepository<TractionTypes,Long> {
    
}
