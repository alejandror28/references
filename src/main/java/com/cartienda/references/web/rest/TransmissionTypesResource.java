package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.TransmissionTypes;

import com.cartienda.references.repository.TransmissionTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TransmissionTypes.
 */
@RestController
@RequestMapping("/api")
public class TransmissionTypesResource {

    private final Logger log = LoggerFactory.getLogger(TransmissionTypesResource.class);

    private static final String ENTITY_NAME = "transmissionTypes";

    private final TransmissionTypesRepository transmissionTypesRepository;

    public TransmissionTypesResource(TransmissionTypesRepository transmissionTypesRepository) {
        this.transmissionTypesRepository = transmissionTypesRepository;
    }

    /**
     * POST  /transmission-types : Create a new transmissionTypes.
     *
     * @param transmissionTypes the transmissionTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transmissionTypes, or with status 400 (Bad Request) if the transmissionTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/transmission-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TransmissionTypes> createTransmissionTypes(@Valid @RequestBody TransmissionTypes transmissionTypes) throws URISyntaxException {
        log.debug("REST request to save TransmissionTypes : {}", transmissionTypes);
        if (transmissionTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new transmissionTypes cannot already have an ID")).body(null);
        }
        TransmissionTypes result = transmissionTypesRepository.save(transmissionTypes);
        return ResponseEntity.created(new URI("/api/transmission-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /transmission-types : Updates an existing transmissionTypes.
     *
     * @param transmissionTypes the transmissionTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated transmissionTypes,
     * or with status 400 (Bad Request) if the transmissionTypes is not valid,
     * or with status 500 (Internal Server Error) if the transmissionTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/transmission-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TransmissionTypes> updateTransmissionTypes(@Valid @RequestBody TransmissionTypes transmissionTypes) throws URISyntaxException {
        log.debug("REST request to update TransmissionTypes : {}", transmissionTypes);
        if (transmissionTypes.getId() == null) {
            return createTransmissionTypes(transmissionTypes);
        }
        TransmissionTypes result = transmissionTypesRepository.save(transmissionTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, transmissionTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /transmission-types : get all the transmissionTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transmissionTypes in body
     */
    @GetMapping("/transmission-types")
    @Timed
    public List<TransmissionTypes> getAllTransmissionTypes() {
        log.debug("REST request to get all TransmissionTypes");
        return transmissionTypesRepository.findAll();
    }

    /**
     * GET  /transmission-types/:id : get the "id" transmissionTypes.
     *
     * @param id the id of the transmissionTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the transmissionTypes, or with status 404 (Not Found)
     */
    @GetMapping("/transmission-types/{id}")
    @Timed
    public ResponseEntity<TransmissionTypes> getTransmissionTypes(@PathVariable Long id) {
        log.debug("REST request to get TransmissionTypes : {}", id);
        TransmissionTypes transmissionTypes = transmissionTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(transmissionTypes));
    }

    /**
     * DELETE  /transmission-types/:id : delete the "id" transmissionTypes.
     *
     * @param id the id of the transmissionTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/transmission-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteTransmissionTypes(@PathVariable Long id) {
        log.debug("REST request to delete TransmissionTypes : {}", id);
        transmissionTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
