package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.FuelTypes;
import com.cartienda.references.repository.FuelTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FuelTypesResource REST controller.
 *
 * @see FuelTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class FuelTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private FuelTypesRepository fuelTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFuelTypesMockMvc;

    private FuelTypes fuelTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FuelTypesResource fuelTypesResource = new FuelTypesResource(fuelTypesRepository);
        this.restFuelTypesMockMvc = MockMvcBuilders.standaloneSetup(fuelTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FuelTypes createEntity(EntityManager em) {
        FuelTypes fuelTypes = new FuelTypes()
            .description(DEFAULT_DESCRIPTION);
        return fuelTypes;
    }

    @Before
    public void initTest() {
        fuelTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createFuelTypes() throws Exception {
        int databaseSizeBeforeCreate = fuelTypesRepository.findAll().size();

        // Create the FuelTypes
        restFuelTypesMockMvc.perform(post("/api/fuel-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTypes)))
            .andExpect(status().isCreated());

        // Validate the FuelTypes in the database
        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeCreate + 1);
        FuelTypes testFuelTypes = fuelTypesList.get(fuelTypesList.size() - 1);
        assertThat(testFuelTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createFuelTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fuelTypesRepository.findAll().size();

        // Create the FuelTypes with an existing ID
        fuelTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFuelTypesMockMvc.perform(post("/api/fuel-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTypesRepository.findAll().size();
        // set the field null
        fuelTypes.setDescription(null);

        // Create the FuelTypes, which fails.

        restFuelTypesMockMvc.perform(post("/api/fuel-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTypes)))
            .andExpect(status().isBadRequest());

        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFuelTypes() throws Exception {
        // Initialize the database
        fuelTypesRepository.saveAndFlush(fuelTypes);

        // Get all the fuelTypesList
        restFuelTypesMockMvc.perform(get("/api/fuel-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fuelTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getFuelTypes() throws Exception {
        // Initialize the database
        fuelTypesRepository.saveAndFlush(fuelTypes);

        // Get the fuelTypes
        restFuelTypesMockMvc.perform(get("/api/fuel-types/{id}", fuelTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fuelTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFuelTypes() throws Exception {
        // Get the fuelTypes
        restFuelTypesMockMvc.perform(get("/api/fuel-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFuelTypes() throws Exception {
        // Initialize the database
        fuelTypesRepository.saveAndFlush(fuelTypes);
        int databaseSizeBeforeUpdate = fuelTypesRepository.findAll().size();

        // Update the fuelTypes
        FuelTypes updatedFuelTypes = fuelTypesRepository.findOne(fuelTypes.getId());
        updatedFuelTypes
            .description(UPDATED_DESCRIPTION);

        restFuelTypesMockMvc.perform(put("/api/fuel-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFuelTypes)))
            .andExpect(status().isOk());

        // Validate the FuelTypes in the database
        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeUpdate);
        FuelTypes testFuelTypes = fuelTypesList.get(fuelTypesList.size() - 1);
        assertThat(testFuelTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingFuelTypes() throws Exception {
        int databaseSizeBeforeUpdate = fuelTypesRepository.findAll().size();

        // Create the FuelTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFuelTypesMockMvc.perform(put("/api/fuel-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTypes)))
            .andExpect(status().isCreated());

        // Validate the FuelTypes in the database
        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFuelTypes() throws Exception {
        // Initialize the database
        fuelTypesRepository.saveAndFlush(fuelTypes);
        int databaseSizeBeforeDelete = fuelTypesRepository.findAll().size();

        // Get the fuelTypes
        restFuelTypesMockMvc.perform(delete("/api/fuel-types/{id}", fuelTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FuelTypes> fuelTypesList = fuelTypesRepository.findAll();
        assertThat(fuelTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FuelTypes.class);
        FuelTypes fuelTypes1 = new FuelTypes();
        fuelTypes1.setId(1L);
        FuelTypes fuelTypes2 = new FuelTypes();
        fuelTypes2.setId(fuelTypes1.getId());
        assertThat(fuelTypes1).isEqualTo(fuelTypes2);
        fuelTypes2.setId(2L);
        assertThat(fuelTypes1).isNotEqualTo(fuelTypes2);
        fuelTypes1.setId(null);
        assertThat(fuelTypes1).isNotEqualTo(fuelTypes2);
    }
}
