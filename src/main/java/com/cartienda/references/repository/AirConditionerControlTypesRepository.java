package com.cartienda.references.repository;

import com.cartienda.references.domain.AirConditionerControlTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AirConditionerControlTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AirConditionerControlTypesRepository extends JpaRepository<AirConditionerControlTypes,Long> {
    
}
