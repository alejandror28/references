package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.TransmissionSpeeds;

import com.cartienda.references.repository.TransmissionSpeedsRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TransmissionSpeeds.
 */
@RestController
@RequestMapping("/api")
public class TransmissionSpeedsResource {

    private final Logger log = LoggerFactory.getLogger(TransmissionSpeedsResource.class);

    private static final String ENTITY_NAME = "transmissionSpeeds";

    private final TransmissionSpeedsRepository transmissionSpeedsRepository;

    public TransmissionSpeedsResource(TransmissionSpeedsRepository transmissionSpeedsRepository) {
        this.transmissionSpeedsRepository = transmissionSpeedsRepository;
    }

    /**
     * POST  /transmission-speeds : Create a new transmissionSpeeds.
     *
     * @param transmissionSpeeds the transmissionSpeeds to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transmissionSpeeds, or with status 400 (Bad Request) if the transmissionSpeeds has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/transmission-speeds")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TransmissionSpeeds> createTransmissionSpeeds(@Valid @RequestBody TransmissionSpeeds transmissionSpeeds) throws URISyntaxException {
        log.debug("REST request to save TransmissionSpeeds : {}", transmissionSpeeds);
        if (transmissionSpeeds.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new transmissionSpeeds cannot already have an ID")).body(null);
        }
        TransmissionSpeeds result = transmissionSpeedsRepository.save(transmissionSpeeds);
        return ResponseEntity.created(new URI("/api/transmission-speeds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /transmission-speeds : Updates an existing transmissionSpeeds.
     *
     * @param transmissionSpeeds the transmissionSpeeds to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated transmissionSpeeds,
     * or with status 400 (Bad Request) if the transmissionSpeeds is not valid,
     * or with status 500 (Internal Server Error) if the transmissionSpeeds couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/transmission-speeds")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TransmissionSpeeds> updateTransmissionSpeeds(@Valid @RequestBody TransmissionSpeeds transmissionSpeeds) throws URISyntaxException {
        log.debug("REST request to update TransmissionSpeeds : {}", transmissionSpeeds);
        if (transmissionSpeeds.getId() == null) {
            return createTransmissionSpeeds(transmissionSpeeds);
        }
        TransmissionSpeeds result = transmissionSpeedsRepository.save(transmissionSpeeds);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, transmissionSpeeds.getId().toString()))
            .body(result);
    }

    /**
     * GET  /transmission-speeds : get all the transmissionSpeeds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transmissionSpeeds in body
     */
    @GetMapping("/transmission-speeds")
    @Timed
    public List<TransmissionSpeeds> getAllTransmissionSpeeds() {
        log.debug("REST request to get all TransmissionSpeeds");
        return transmissionSpeedsRepository.findAll();
    }

    /**
     * GET  /transmission-speeds/:id : get the "id" transmissionSpeeds.
     *
     * @param id the id of the transmissionSpeeds to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the transmissionSpeeds, or with status 404 (Not Found)
     */
    @GetMapping("/transmission-speeds/{id}")
    @Timed
    public ResponseEntity<TransmissionSpeeds> getTransmissionSpeeds(@PathVariable Long id) {
        log.debug("REST request to get TransmissionSpeeds : {}", id);
        TransmissionSpeeds transmissionSpeeds = transmissionSpeedsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(transmissionSpeeds));
    }

    /**
     * DELETE  /transmission-speeds/:id : delete the "id" transmissionSpeeds.
     *
     * @param id the id of the transmissionSpeeds to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/transmission-speeds/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteTransmissionSpeeds(@PathVariable Long id) {
        log.debug("REST request to delete TransmissionSpeeds : {}", id);
        transmissionSpeedsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
