package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.OwnerDocumentTypes;

import com.cartienda.references.repository.OwnerDocumentTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OwnerDocumentTypes.
 */
@RestController
@RequestMapping("/api")
public class OwnerDocumentTypesResource {

    private final Logger log = LoggerFactory.getLogger(OwnerDocumentTypesResource.class);

    private static final String ENTITY_NAME = "ownerDocumentTypes";

    private final OwnerDocumentTypesRepository ownerDocumentTypesRepository;

    public OwnerDocumentTypesResource(OwnerDocumentTypesRepository ownerDocumentTypesRepository) {
        this.ownerDocumentTypesRepository = ownerDocumentTypesRepository;
    }

    /**
     * POST  /owner-document-types : Create a new ownerDocumentTypes.
     *
     * @param ownerDocumentTypes the ownerDocumentTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ownerDocumentTypes, or with status 400 (Bad Request) if the ownerDocumentTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/owner-document-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<OwnerDocumentTypes> createOwnerDocumentTypes(@Valid @RequestBody OwnerDocumentTypes ownerDocumentTypes) throws URISyntaxException {
        log.debug("REST request to save OwnerDocumentTypes : {}", ownerDocumentTypes);
        if (ownerDocumentTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ownerDocumentTypes cannot already have an ID")).body(null);
        }
        OwnerDocumentTypes result = ownerDocumentTypesRepository.save(ownerDocumentTypes);
        return ResponseEntity.created(new URI("/api/owner-document-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /owner-document-types : Updates an existing ownerDocumentTypes.
     *
     * @param ownerDocumentTypes the ownerDocumentTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ownerDocumentTypes,
     * or with status 400 (Bad Request) if the ownerDocumentTypes is not valid,
     * or with status 500 (Internal Server Error) if the ownerDocumentTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/owner-document-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<OwnerDocumentTypes> updateOwnerDocumentTypes(@Valid @RequestBody OwnerDocumentTypes ownerDocumentTypes) throws URISyntaxException {
        log.debug("REST request to update OwnerDocumentTypes : {}", ownerDocumentTypes);
        if (ownerDocumentTypes.getId() == null) {
            return createOwnerDocumentTypes(ownerDocumentTypes);
        }
        OwnerDocumentTypes result = ownerDocumentTypesRepository.save(ownerDocumentTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ownerDocumentTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /owner-document-types : get all the ownerDocumentTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ownerDocumentTypes in body
     */
    @GetMapping("/owner-document-types")
    @Timed
    public List<OwnerDocumentTypes> getAllOwnerDocumentTypes() {
        log.debug("REST request to get all OwnerDocumentTypes");
        return ownerDocumentTypesRepository.findAll();
    }

    /**
     * GET  /owner-document-types/:id : get the "id" ownerDocumentTypes.
     *
     * @param id the id of the ownerDocumentTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ownerDocumentTypes, or with status 404 (Not Found)
     */
    @GetMapping("/owner-document-types/{id}")
    @Timed
    public ResponseEntity<OwnerDocumentTypes> getOwnerDocumentTypes(@PathVariable Long id) {
        log.debug("REST request to get OwnerDocumentTypes : {}", id);
        OwnerDocumentTypes ownerDocumentTypes = ownerDocumentTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ownerDocumentTypes));
    }

    /**
     * DELETE  /owner-document-types/:id : delete the "id" ownerDocumentTypes.
     *
     * @param id the id of the ownerDocumentTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/owner-document-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteOwnerDocumentTypes(@PathVariable Long id) {
        log.debug("REST request to delete OwnerDocumentTypes : {}", id);
        ownerDocumentTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
