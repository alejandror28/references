package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.UpholsteryTypes;
import com.cartienda.references.repository.UpholsteryTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UpholsteryTypesResource REST controller.
 *
 * @see UpholsteryTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class UpholsteryTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private UpholsteryTypesRepository upholsteryTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUpholsteryTypesMockMvc;

    private UpholsteryTypes upholsteryTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UpholsteryTypesResource upholsteryTypesResource = new UpholsteryTypesResource(upholsteryTypesRepository);
        this.restUpholsteryTypesMockMvc = MockMvcBuilders.standaloneSetup(upholsteryTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UpholsteryTypes createEntity(EntityManager em) {
        UpholsteryTypes upholsteryTypes = new UpholsteryTypes()
            .description(DEFAULT_DESCRIPTION);
        return upholsteryTypes;
    }

    @Before
    public void initTest() {
        upholsteryTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createUpholsteryTypes() throws Exception {
        int databaseSizeBeforeCreate = upholsteryTypesRepository.findAll().size();

        // Create the UpholsteryTypes
        restUpholsteryTypesMockMvc.perform(post("/api/upholstery-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(upholsteryTypes)))
            .andExpect(status().isCreated());

        // Validate the UpholsteryTypes in the database
        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeCreate + 1);
        UpholsteryTypes testUpholsteryTypes = upholsteryTypesList.get(upholsteryTypesList.size() - 1);
        assertThat(testUpholsteryTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createUpholsteryTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = upholsteryTypesRepository.findAll().size();

        // Create the UpholsteryTypes with an existing ID
        upholsteryTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUpholsteryTypesMockMvc.perform(post("/api/upholstery-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(upholsteryTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = upholsteryTypesRepository.findAll().size();
        // set the field null
        upholsteryTypes.setDescription(null);

        // Create the UpholsteryTypes, which fails.

        restUpholsteryTypesMockMvc.perform(post("/api/upholstery-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(upholsteryTypes)))
            .andExpect(status().isBadRequest());

        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUpholsteryTypes() throws Exception {
        // Initialize the database
        upholsteryTypesRepository.saveAndFlush(upholsteryTypes);

        // Get all the upholsteryTypesList
        restUpholsteryTypesMockMvc.perform(get("/api/upholstery-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(upholsteryTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getUpholsteryTypes() throws Exception {
        // Initialize the database
        upholsteryTypesRepository.saveAndFlush(upholsteryTypes);

        // Get the upholsteryTypes
        restUpholsteryTypesMockMvc.perform(get("/api/upholstery-types/{id}", upholsteryTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(upholsteryTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUpholsteryTypes() throws Exception {
        // Get the upholsteryTypes
        restUpholsteryTypesMockMvc.perform(get("/api/upholstery-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUpholsteryTypes() throws Exception {
        // Initialize the database
        upholsteryTypesRepository.saveAndFlush(upholsteryTypes);
        int databaseSizeBeforeUpdate = upholsteryTypesRepository.findAll().size();

        // Update the upholsteryTypes
        UpholsteryTypes updatedUpholsteryTypes = upholsteryTypesRepository.findOne(upholsteryTypes.getId());
        updatedUpholsteryTypes
            .description(UPDATED_DESCRIPTION);

        restUpholsteryTypesMockMvc.perform(put("/api/upholstery-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUpholsteryTypes)))
            .andExpect(status().isOk());

        // Validate the UpholsteryTypes in the database
        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeUpdate);
        UpholsteryTypes testUpholsteryTypes = upholsteryTypesList.get(upholsteryTypesList.size() - 1);
        assertThat(testUpholsteryTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingUpholsteryTypes() throws Exception {
        int databaseSizeBeforeUpdate = upholsteryTypesRepository.findAll().size();

        // Create the UpholsteryTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUpholsteryTypesMockMvc.perform(put("/api/upholstery-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(upholsteryTypes)))
            .andExpect(status().isCreated());

        // Validate the UpholsteryTypes in the database
        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUpholsteryTypes() throws Exception {
        // Initialize the database
        upholsteryTypesRepository.saveAndFlush(upholsteryTypes);
        int databaseSizeBeforeDelete = upholsteryTypesRepository.findAll().size();

        // Get the upholsteryTypes
        restUpholsteryTypesMockMvc.perform(delete("/api/upholstery-types/{id}", upholsteryTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UpholsteryTypes> upholsteryTypesList = upholsteryTypesRepository.findAll();
        assertThat(upholsteryTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UpholsteryTypes.class);
        UpholsteryTypes upholsteryTypes1 = new UpholsteryTypes();
        upholsteryTypes1.setId(1L);
        UpholsteryTypes upholsteryTypes2 = new UpholsteryTypes();
        upholsteryTypes2.setId(upholsteryTypes1.getId());
        assertThat(upholsteryTypes1).isEqualTo(upholsteryTypes2);
        upholsteryTypes2.setId(2L);
        assertThat(upholsteryTypes1).isNotEqualTo(upholsteryTypes2);
        upholsteryTypes1.setId(null);
        assertThat(upholsteryTypes1).isNotEqualTo(upholsteryTypes2);
    }
}
