package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.WindowsSystemTypes;

import com.cartienda.references.repository.WindowsSystemTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing WindowsSystemTypes.
 */
@RestController
@RequestMapping("/api")
public class WindowsSystemTypesResource {

    private final Logger log = LoggerFactory.getLogger(WindowsSystemTypesResource.class);

    private static final String ENTITY_NAME = "windowsSystemTypes";

    private final WindowsSystemTypesRepository windowsSystemTypesRepository;

    public WindowsSystemTypesResource(WindowsSystemTypesRepository windowsSystemTypesRepository) {
        this.windowsSystemTypesRepository = windowsSystemTypesRepository;
    }

    /**
     * POST  /windows-system-types : Create a new windowsSystemTypes.
     *
     * @param windowsSystemTypes the windowsSystemTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new windowsSystemTypes, or with status 400 (Bad Request) if the windowsSystemTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/windows-system-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<WindowsSystemTypes> createWindowsSystemTypes(@Valid @RequestBody WindowsSystemTypes windowsSystemTypes) throws URISyntaxException {
        log.debug("REST request to save WindowsSystemTypes : {}", windowsSystemTypes);
        if (windowsSystemTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new windowsSystemTypes cannot already have an ID")).body(null);
        }
        WindowsSystemTypes result = windowsSystemTypesRepository.save(windowsSystemTypes);
        return ResponseEntity.created(new URI("/api/windows-system-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /windows-system-types : Updates an existing windowsSystemTypes.
     *
     * @param windowsSystemTypes the windowsSystemTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated windowsSystemTypes,
     * or with status 400 (Bad Request) if the windowsSystemTypes is not valid,
     * or with status 500 (Internal Server Error) if the windowsSystemTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/windows-system-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<WindowsSystemTypes> updateWindowsSystemTypes(@Valid @RequestBody WindowsSystemTypes windowsSystemTypes) throws URISyntaxException {
        log.debug("REST request to update WindowsSystemTypes : {}", windowsSystemTypes);
        if (windowsSystemTypes.getId() == null) {
            return createWindowsSystemTypes(windowsSystemTypes);
        }
        WindowsSystemTypes result = windowsSystemTypesRepository.save(windowsSystemTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, windowsSystemTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /windows-system-types : get all the windowsSystemTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of windowsSystemTypes in body
     */
    @GetMapping("/windows-system-types")
    @Timed
    public List<WindowsSystemTypes> getAllWindowsSystemTypes() {
        log.debug("REST request to get all WindowsSystemTypes");
        return windowsSystemTypesRepository.findAll();
    }

    /**
     * GET  /windows-system-types/:id : get the "id" windowsSystemTypes.
     *
     * @param id the id of the windowsSystemTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the windowsSystemTypes, or with status 404 (Not Found)
     */
    @GetMapping("/windows-system-types/{id}")
    @Timed
    public ResponseEntity<WindowsSystemTypes> getWindowsSystemTypes(@PathVariable Long id) {
        log.debug("REST request to get WindowsSystemTypes : {}", id);
        WindowsSystemTypes windowsSystemTypes = windowsSystemTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(windowsSystemTypes));
    }

    /**
     * DELETE  /windows-system-types/:id : delete the "id" windowsSystemTypes.
     *
     * @param id the id of the windowsSystemTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/windows-system-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteWindowsSystemTypes(@PathVariable Long id) {
        log.debug("REST request to delete WindowsSystemTypes : {}", id);
        windowsSystemTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
