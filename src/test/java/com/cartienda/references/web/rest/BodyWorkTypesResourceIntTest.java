package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.BodyWorkTypes;
import com.cartienda.references.repository.BodyWorkTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BodyWorkTypesResource REST controller.
 *
 * @see BodyWorkTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class BodyWorkTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private BodyWorkTypesRepository bodyWorkTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBodyWorkTypesMockMvc;

    private BodyWorkTypes bodyWorkTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BodyWorkTypesResource bodyWorkTypesResource = new BodyWorkTypesResource(bodyWorkTypesRepository);
        this.restBodyWorkTypesMockMvc = MockMvcBuilders.standaloneSetup(bodyWorkTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BodyWorkTypes createEntity(EntityManager em) {
        BodyWorkTypes bodyWorkTypes = new BodyWorkTypes()
            .description(DEFAULT_DESCRIPTION);
        return bodyWorkTypes;
    }

    @Before
    public void initTest() {
        bodyWorkTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createBodyWorkTypes() throws Exception {
        int databaseSizeBeforeCreate = bodyWorkTypesRepository.findAll().size();

        // Create the BodyWorkTypes
        restBodyWorkTypesMockMvc.perform(post("/api/body-work-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bodyWorkTypes)))
            .andExpect(status().isCreated());

        // Validate the BodyWorkTypes in the database
        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeCreate + 1);
        BodyWorkTypes testBodyWorkTypes = bodyWorkTypesList.get(bodyWorkTypesList.size() - 1);
        assertThat(testBodyWorkTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createBodyWorkTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bodyWorkTypesRepository.findAll().size();

        // Create the BodyWorkTypes with an existing ID
        bodyWorkTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBodyWorkTypesMockMvc.perform(post("/api/body-work-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bodyWorkTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = bodyWorkTypesRepository.findAll().size();
        // set the field null
        bodyWorkTypes.setDescription(null);

        // Create the BodyWorkTypes, which fails.

        restBodyWorkTypesMockMvc.perform(post("/api/body-work-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bodyWorkTypes)))
            .andExpect(status().isBadRequest());

        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBodyWorkTypes() throws Exception {
        // Initialize the database
        bodyWorkTypesRepository.saveAndFlush(bodyWorkTypes);

        // Get all the bodyWorkTypesList
        restBodyWorkTypesMockMvc.perform(get("/api/body-work-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bodyWorkTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getBodyWorkTypes() throws Exception {
        // Initialize the database
        bodyWorkTypesRepository.saveAndFlush(bodyWorkTypes);

        // Get the bodyWorkTypes
        restBodyWorkTypesMockMvc.perform(get("/api/body-work-types/{id}", bodyWorkTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bodyWorkTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBodyWorkTypes() throws Exception {
        // Get the bodyWorkTypes
        restBodyWorkTypesMockMvc.perform(get("/api/body-work-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBodyWorkTypes() throws Exception {
        // Initialize the database
        bodyWorkTypesRepository.saveAndFlush(bodyWorkTypes);
        int databaseSizeBeforeUpdate = bodyWorkTypesRepository.findAll().size();

        // Update the bodyWorkTypes
        BodyWorkTypes updatedBodyWorkTypes = bodyWorkTypesRepository.findOne(bodyWorkTypes.getId());
        updatedBodyWorkTypes
            .description(UPDATED_DESCRIPTION);

        restBodyWorkTypesMockMvc.perform(put("/api/body-work-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBodyWorkTypes)))
            .andExpect(status().isOk());

        // Validate the BodyWorkTypes in the database
        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeUpdate);
        BodyWorkTypes testBodyWorkTypes = bodyWorkTypesList.get(bodyWorkTypesList.size() - 1);
        assertThat(testBodyWorkTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingBodyWorkTypes() throws Exception {
        int databaseSizeBeforeUpdate = bodyWorkTypesRepository.findAll().size();

        // Create the BodyWorkTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBodyWorkTypesMockMvc.perform(put("/api/body-work-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bodyWorkTypes)))
            .andExpect(status().isCreated());

        // Validate the BodyWorkTypes in the database
        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBodyWorkTypes() throws Exception {
        // Initialize the database
        bodyWorkTypesRepository.saveAndFlush(bodyWorkTypes);
        int databaseSizeBeforeDelete = bodyWorkTypesRepository.findAll().size();

        // Get the bodyWorkTypes
        restBodyWorkTypesMockMvc.perform(delete("/api/body-work-types/{id}", bodyWorkTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BodyWorkTypes> bodyWorkTypesList = bodyWorkTypesRepository.findAll();
        assertThat(bodyWorkTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BodyWorkTypes.class);
        BodyWorkTypes bodyWorkTypes1 = new BodyWorkTypes();
        bodyWorkTypes1.setId(1L);
        BodyWorkTypes bodyWorkTypes2 = new BodyWorkTypes();
        bodyWorkTypes2.setId(bodyWorkTypes1.getId());
        assertThat(bodyWorkTypes1).isEqualTo(bodyWorkTypes2);
        bodyWorkTypes2.setId(2L);
        assertThat(bodyWorkTypes1).isNotEqualTo(bodyWorkTypes2);
        bodyWorkTypes1.setId(null);
        assertThat(bodyWorkTypes1).isNotEqualTo(bodyWorkTypes2);
    }
}
