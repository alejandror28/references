package com.cartienda.references.repository;

import com.cartienda.references.domain.VehicleStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VehicleStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleStatusRepository extends JpaRepository<VehicleStatus,Long> {
    
}
