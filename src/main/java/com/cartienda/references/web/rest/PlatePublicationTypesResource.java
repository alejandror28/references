package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.PlatePublicationTypes;

import com.cartienda.references.repository.PlatePublicationTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PlatePublicationTypes.
 */
@RestController
@RequestMapping("/api")
public class PlatePublicationTypesResource {

    private final Logger log = LoggerFactory.getLogger(PlatePublicationTypesResource.class);

    private static final String ENTITY_NAME = "platePublicationTypes";

    private final PlatePublicationTypesRepository platePublicationTypesRepository;

    public PlatePublicationTypesResource(PlatePublicationTypesRepository platePublicationTypesRepository) {
        this.platePublicationTypesRepository = platePublicationTypesRepository;
    }

    /**
     * POST  /plate-publication-types : Create a new platePublicationTypes.
     *
     * @param platePublicationTypes the platePublicationTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new platePublicationTypes, or with status 400 (Bad Request) if the platePublicationTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/plate-publication-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<PlatePublicationTypes> createPlatePublicationTypes(@Valid @RequestBody PlatePublicationTypes platePublicationTypes) throws URISyntaxException {
        log.debug("REST request to save PlatePublicationTypes : {}", platePublicationTypes);
        if (platePublicationTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new platePublicationTypes cannot already have an ID")).body(null);
        }
        PlatePublicationTypes result = platePublicationTypesRepository.save(platePublicationTypes);
        return ResponseEntity.created(new URI("/api/plate-publication-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /plate-publication-types : Updates an existing platePublicationTypes.
     *
     * @param platePublicationTypes the platePublicationTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated platePublicationTypes,
     * or with status 400 (Bad Request) if the platePublicationTypes is not valid,
     * or with status 500 (Internal Server Error) if the platePublicationTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/plate-publication-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<PlatePublicationTypes> updatePlatePublicationTypes(@Valid @RequestBody PlatePublicationTypes platePublicationTypes) throws URISyntaxException {
        log.debug("REST request to update PlatePublicationTypes : {}", platePublicationTypes);
        if (platePublicationTypes.getId() == null) {
            return createPlatePublicationTypes(platePublicationTypes);
        }
        PlatePublicationTypes result = platePublicationTypesRepository.save(platePublicationTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, platePublicationTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /plate-publication-types : get all the platePublicationTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of platePublicationTypes in body
     */
    @GetMapping("/plate-publication-types")
    @Timed
    public List<PlatePublicationTypes> getAllPlatePublicationTypes() {
        log.debug("REST request to get all PlatePublicationTypes");
        return platePublicationTypesRepository.findAll();
    }

    /**
     * GET  /plate-publication-types/:id : get the "id" platePublicationTypes.
     *
     * @param id the id of the platePublicationTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the platePublicationTypes, or with status 404 (Not Found)
     */
    @GetMapping("/plate-publication-types/{id}")
    @Timed
    public ResponseEntity<PlatePublicationTypes> getPlatePublicationTypes(@PathVariable Long id) {
        log.debug("REST request to get PlatePublicationTypes : {}", id);
        PlatePublicationTypes platePublicationTypes = platePublicationTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(platePublicationTypes));
    }

    /**
     * DELETE  /plate-publication-types/:id : delete the "id" platePublicationTypes.
     *
     * @param id the id of the platePublicationTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/plate-publication-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deletePlatePublicationTypes(@PathVariable Long id) {
        log.debug("REST request to delete PlatePublicationTypes : {}", id);
        platePublicationTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
