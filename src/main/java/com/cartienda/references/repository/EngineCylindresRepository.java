package com.cartienda.references.repository;

import com.cartienda.references.domain.EngineCylindres;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EngineCylindres entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngineCylindresRepository extends JpaRepository<EngineCylindres,Long> {
    
}
