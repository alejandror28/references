package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.SunroofTypes;
import com.cartienda.references.repository.SunroofTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SunroofTypesResource REST controller.
 *
 * @see SunroofTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class SunroofTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private SunroofTypesRepository sunroofTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSunroofTypesMockMvc;

    private SunroofTypes sunroofTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SunroofTypesResource sunroofTypesResource = new SunroofTypesResource(sunroofTypesRepository);
        this.restSunroofTypesMockMvc = MockMvcBuilders.standaloneSetup(sunroofTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SunroofTypes createEntity(EntityManager em) {
        SunroofTypes sunroofTypes = new SunroofTypes()
            .description(DEFAULT_DESCRIPTION);
        return sunroofTypes;
    }

    @Before
    public void initTest() {
        sunroofTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createSunroofTypes() throws Exception {
        int databaseSizeBeforeCreate = sunroofTypesRepository.findAll().size();

        // Create the SunroofTypes
        restSunroofTypesMockMvc.perform(post("/api/sunroof-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sunroofTypes)))
            .andExpect(status().isCreated());

        // Validate the SunroofTypes in the database
        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeCreate + 1);
        SunroofTypes testSunroofTypes = sunroofTypesList.get(sunroofTypesList.size() - 1);
        assertThat(testSunroofTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createSunroofTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sunroofTypesRepository.findAll().size();

        // Create the SunroofTypes with an existing ID
        sunroofTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSunroofTypesMockMvc.perform(post("/api/sunroof-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sunroofTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = sunroofTypesRepository.findAll().size();
        // set the field null
        sunroofTypes.setDescription(null);

        // Create the SunroofTypes, which fails.

        restSunroofTypesMockMvc.perform(post("/api/sunroof-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sunroofTypes)))
            .andExpect(status().isBadRequest());

        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSunroofTypes() throws Exception {
        // Initialize the database
        sunroofTypesRepository.saveAndFlush(sunroofTypes);

        // Get all the sunroofTypesList
        restSunroofTypesMockMvc.perform(get("/api/sunroof-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sunroofTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getSunroofTypes() throws Exception {
        // Initialize the database
        sunroofTypesRepository.saveAndFlush(sunroofTypes);

        // Get the sunroofTypes
        restSunroofTypesMockMvc.perform(get("/api/sunroof-types/{id}", sunroofTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sunroofTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSunroofTypes() throws Exception {
        // Get the sunroofTypes
        restSunroofTypesMockMvc.perform(get("/api/sunroof-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSunroofTypes() throws Exception {
        // Initialize the database
        sunroofTypesRepository.saveAndFlush(sunroofTypes);
        int databaseSizeBeforeUpdate = sunroofTypesRepository.findAll().size();

        // Update the sunroofTypes
        SunroofTypes updatedSunroofTypes = sunroofTypesRepository.findOne(sunroofTypes.getId());
        updatedSunroofTypes
            .description(UPDATED_DESCRIPTION);

        restSunroofTypesMockMvc.perform(put("/api/sunroof-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSunroofTypes)))
            .andExpect(status().isOk());

        // Validate the SunroofTypes in the database
        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeUpdate);
        SunroofTypes testSunroofTypes = sunroofTypesList.get(sunroofTypesList.size() - 1);
        assertThat(testSunroofTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingSunroofTypes() throws Exception {
        int databaseSizeBeforeUpdate = sunroofTypesRepository.findAll().size();

        // Create the SunroofTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSunroofTypesMockMvc.perform(put("/api/sunroof-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sunroofTypes)))
            .andExpect(status().isCreated());

        // Validate the SunroofTypes in the database
        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSunroofTypes() throws Exception {
        // Initialize the database
        sunroofTypesRepository.saveAndFlush(sunroofTypes);
        int databaseSizeBeforeDelete = sunroofTypesRepository.findAll().size();

        // Get the sunroofTypes
        restSunroofTypesMockMvc.perform(delete("/api/sunroof-types/{id}", sunroofTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SunroofTypes> sunroofTypesList = sunroofTypesRepository.findAll();
        assertThat(sunroofTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SunroofTypes.class);
        SunroofTypes sunroofTypes1 = new SunroofTypes();
        sunroofTypes1.setId(1L);
        SunroofTypes sunroofTypes2 = new SunroofTypes();
        sunroofTypes2.setId(sunroofTypes1.getId());
        assertThat(sunroofTypes1).isEqualTo(sunroofTypes2);
        sunroofTypes2.setId(2L);
        assertThat(sunroofTypes1).isNotEqualTo(sunroofTypes2);
        sunroofTypes1.setId(null);
        assertThat(sunroofTypes1).isNotEqualTo(sunroofTypes2);
    }
}
