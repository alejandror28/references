package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.WindowsSystemTypes;
import com.cartienda.references.repository.WindowsSystemTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the WindowsSystemTypesResource REST controller.
 *
 * @see WindowsSystemTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class WindowsSystemTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private WindowsSystemTypesRepository windowsSystemTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restWindowsSystemTypesMockMvc;

    private WindowsSystemTypes windowsSystemTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        WindowsSystemTypesResource windowsSystemTypesResource = new WindowsSystemTypesResource(windowsSystemTypesRepository);
        this.restWindowsSystemTypesMockMvc = MockMvcBuilders.standaloneSetup(windowsSystemTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WindowsSystemTypes createEntity(EntityManager em) {
        WindowsSystemTypes windowsSystemTypes = new WindowsSystemTypes()
            .description(DEFAULT_DESCRIPTION);
        return windowsSystemTypes;
    }

    @Before
    public void initTest() {
        windowsSystemTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createWindowsSystemTypes() throws Exception {
        int databaseSizeBeforeCreate = windowsSystemTypesRepository.findAll().size();

        // Create the WindowsSystemTypes
        restWindowsSystemTypesMockMvc.perform(post("/api/windows-system-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(windowsSystemTypes)))
            .andExpect(status().isCreated());

        // Validate the WindowsSystemTypes in the database
        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeCreate + 1);
        WindowsSystemTypes testWindowsSystemTypes = windowsSystemTypesList.get(windowsSystemTypesList.size() - 1);
        assertThat(testWindowsSystemTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createWindowsSystemTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = windowsSystemTypesRepository.findAll().size();

        // Create the WindowsSystemTypes with an existing ID
        windowsSystemTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWindowsSystemTypesMockMvc.perform(post("/api/windows-system-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(windowsSystemTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = windowsSystemTypesRepository.findAll().size();
        // set the field null
        windowsSystemTypes.setDescription(null);

        // Create the WindowsSystemTypes, which fails.

        restWindowsSystemTypesMockMvc.perform(post("/api/windows-system-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(windowsSystemTypes)))
            .andExpect(status().isBadRequest());

        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWindowsSystemTypes() throws Exception {
        // Initialize the database
        windowsSystemTypesRepository.saveAndFlush(windowsSystemTypes);

        // Get all the windowsSystemTypesList
        restWindowsSystemTypesMockMvc.perform(get("/api/windows-system-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(windowsSystemTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getWindowsSystemTypes() throws Exception {
        // Initialize the database
        windowsSystemTypesRepository.saveAndFlush(windowsSystemTypes);

        // Get the windowsSystemTypes
        restWindowsSystemTypesMockMvc.perform(get("/api/windows-system-types/{id}", windowsSystemTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(windowsSystemTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingWindowsSystemTypes() throws Exception {
        // Get the windowsSystemTypes
        restWindowsSystemTypesMockMvc.perform(get("/api/windows-system-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWindowsSystemTypes() throws Exception {
        // Initialize the database
        windowsSystemTypesRepository.saveAndFlush(windowsSystemTypes);
        int databaseSizeBeforeUpdate = windowsSystemTypesRepository.findAll().size();

        // Update the windowsSystemTypes
        WindowsSystemTypes updatedWindowsSystemTypes = windowsSystemTypesRepository.findOne(windowsSystemTypes.getId());
        updatedWindowsSystemTypes
            .description(UPDATED_DESCRIPTION);

        restWindowsSystemTypesMockMvc.perform(put("/api/windows-system-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWindowsSystemTypes)))
            .andExpect(status().isOk());

        // Validate the WindowsSystemTypes in the database
        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeUpdate);
        WindowsSystemTypes testWindowsSystemTypes = windowsSystemTypesList.get(windowsSystemTypesList.size() - 1);
        assertThat(testWindowsSystemTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingWindowsSystemTypes() throws Exception {
        int databaseSizeBeforeUpdate = windowsSystemTypesRepository.findAll().size();

        // Create the WindowsSystemTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restWindowsSystemTypesMockMvc.perform(put("/api/windows-system-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(windowsSystemTypes)))
            .andExpect(status().isCreated());

        // Validate the WindowsSystemTypes in the database
        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteWindowsSystemTypes() throws Exception {
        // Initialize the database
        windowsSystemTypesRepository.saveAndFlush(windowsSystemTypes);
        int databaseSizeBeforeDelete = windowsSystemTypesRepository.findAll().size();

        // Get the windowsSystemTypes
        restWindowsSystemTypesMockMvc.perform(delete("/api/windows-system-types/{id}", windowsSystemTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<WindowsSystemTypes> windowsSystemTypesList = windowsSystemTypesRepository.findAll();
        assertThat(windowsSystemTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WindowsSystemTypes.class);
        WindowsSystemTypes windowsSystemTypes1 = new WindowsSystemTypes();
        windowsSystemTypes1.setId(1L);
        WindowsSystemTypes windowsSystemTypes2 = new WindowsSystemTypes();
        windowsSystemTypes2.setId(windowsSystemTypes1.getId());
        assertThat(windowsSystemTypes1).isEqualTo(windowsSystemTypes2);
        windowsSystemTypes2.setId(2L);
        assertThat(windowsSystemTypes1).isNotEqualTo(windowsSystemTypes2);
        windowsSystemTypes1.setId(null);
        assertThat(windowsSystemTypes1).isNotEqualTo(windowsSystemTypes2);
    }
}
