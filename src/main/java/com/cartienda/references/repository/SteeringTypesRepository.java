package com.cartienda.references.repository;

import com.cartienda.references.domain.SteeringTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SteeringTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SteeringTypesRepository extends JpaRepository<SteeringTypes,Long> {
    
}
