package com.cartienda.references.repository;

import com.cartienda.references.domain.TransmissionTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TransmissionTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransmissionTypesRepository extends JpaRepository<TransmissionTypes,Long> {
    
}
