package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.OwnerDocumentTypes;
import com.cartienda.references.repository.OwnerDocumentTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OwnerDocumentTypesResource REST controller.
 *
 * @see OwnerDocumentTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class OwnerDocumentTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private OwnerDocumentTypesRepository ownerDocumentTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOwnerDocumentTypesMockMvc;

    private OwnerDocumentTypes ownerDocumentTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OwnerDocumentTypesResource ownerDocumentTypesResource = new OwnerDocumentTypesResource(ownerDocumentTypesRepository);
        this.restOwnerDocumentTypesMockMvc = MockMvcBuilders.standaloneSetup(ownerDocumentTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OwnerDocumentTypes createEntity(EntityManager em) {
        OwnerDocumentTypes ownerDocumentTypes = new OwnerDocumentTypes()
            .description(DEFAULT_DESCRIPTION);
        return ownerDocumentTypes;
    }

    @Before
    public void initTest() {
        ownerDocumentTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createOwnerDocumentTypes() throws Exception {
        int databaseSizeBeforeCreate = ownerDocumentTypesRepository.findAll().size();

        // Create the OwnerDocumentTypes
        restOwnerDocumentTypesMockMvc.perform(post("/api/owner-document-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ownerDocumentTypes)))
            .andExpect(status().isCreated());

        // Validate the OwnerDocumentTypes in the database
        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeCreate + 1);
        OwnerDocumentTypes testOwnerDocumentTypes = ownerDocumentTypesList.get(ownerDocumentTypesList.size() - 1);
        assertThat(testOwnerDocumentTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createOwnerDocumentTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ownerDocumentTypesRepository.findAll().size();

        // Create the OwnerDocumentTypes with an existing ID
        ownerDocumentTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOwnerDocumentTypesMockMvc.perform(post("/api/owner-document-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ownerDocumentTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = ownerDocumentTypesRepository.findAll().size();
        // set the field null
        ownerDocumentTypes.setDescription(null);

        // Create the OwnerDocumentTypes, which fails.

        restOwnerDocumentTypesMockMvc.perform(post("/api/owner-document-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ownerDocumentTypes)))
            .andExpect(status().isBadRequest());

        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOwnerDocumentTypes() throws Exception {
        // Initialize the database
        ownerDocumentTypesRepository.saveAndFlush(ownerDocumentTypes);

        // Get all the ownerDocumentTypesList
        restOwnerDocumentTypesMockMvc.perform(get("/api/owner-document-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ownerDocumentTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getOwnerDocumentTypes() throws Exception {
        // Initialize the database
        ownerDocumentTypesRepository.saveAndFlush(ownerDocumentTypes);

        // Get the ownerDocumentTypes
        restOwnerDocumentTypesMockMvc.perform(get("/api/owner-document-types/{id}", ownerDocumentTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ownerDocumentTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOwnerDocumentTypes() throws Exception {
        // Get the ownerDocumentTypes
        restOwnerDocumentTypesMockMvc.perform(get("/api/owner-document-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOwnerDocumentTypes() throws Exception {
        // Initialize the database
        ownerDocumentTypesRepository.saveAndFlush(ownerDocumentTypes);
        int databaseSizeBeforeUpdate = ownerDocumentTypesRepository.findAll().size();

        // Update the ownerDocumentTypes
        OwnerDocumentTypes updatedOwnerDocumentTypes = ownerDocumentTypesRepository.findOne(ownerDocumentTypes.getId());
        updatedOwnerDocumentTypes
            .description(UPDATED_DESCRIPTION);

        restOwnerDocumentTypesMockMvc.perform(put("/api/owner-document-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOwnerDocumentTypes)))
            .andExpect(status().isOk());

        // Validate the OwnerDocumentTypes in the database
        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeUpdate);
        OwnerDocumentTypes testOwnerDocumentTypes = ownerDocumentTypesList.get(ownerDocumentTypesList.size() - 1);
        assertThat(testOwnerDocumentTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingOwnerDocumentTypes() throws Exception {
        int databaseSizeBeforeUpdate = ownerDocumentTypesRepository.findAll().size();

        // Create the OwnerDocumentTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOwnerDocumentTypesMockMvc.perform(put("/api/owner-document-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ownerDocumentTypes)))
            .andExpect(status().isCreated());

        // Validate the OwnerDocumentTypes in the database
        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOwnerDocumentTypes() throws Exception {
        // Initialize the database
        ownerDocumentTypesRepository.saveAndFlush(ownerDocumentTypes);
        int databaseSizeBeforeDelete = ownerDocumentTypesRepository.findAll().size();

        // Get the ownerDocumentTypes
        restOwnerDocumentTypesMockMvc.perform(delete("/api/owner-document-types/{id}", ownerDocumentTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OwnerDocumentTypes> ownerDocumentTypesList = ownerDocumentTypesRepository.findAll();
        assertThat(ownerDocumentTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OwnerDocumentTypes.class);
        OwnerDocumentTypes ownerDocumentTypes1 = new OwnerDocumentTypes();
        ownerDocumentTypes1.setId(1L);
        OwnerDocumentTypes ownerDocumentTypes2 = new OwnerDocumentTypes();
        ownerDocumentTypes2.setId(ownerDocumentTypes1.getId());
        assertThat(ownerDocumentTypes1).isEqualTo(ownerDocumentTypes2);
        ownerDocumentTypes2.setId(2L);
        assertThat(ownerDocumentTypes1).isNotEqualTo(ownerDocumentTypes2);
        ownerDocumentTypes1.setId(null);
        assertThat(ownerDocumentTypes1).isNotEqualTo(ownerDocumentTypes2);
    }
}
