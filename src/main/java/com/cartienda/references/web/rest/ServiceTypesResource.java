package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.ServiceTypes;

import com.cartienda.references.repository.ServiceTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceTypes.
 */
@RestController
@RequestMapping("/api")
public class ServiceTypesResource {

    private final Logger log = LoggerFactory.getLogger(ServiceTypesResource.class);

    private static final String ENTITY_NAME = "serviceTypes";

    private final ServiceTypesRepository serviceTypesRepository;

    public ServiceTypesResource(ServiceTypesRepository serviceTypesRepository) {
        this.serviceTypesRepository = serviceTypesRepository;
    }

    /**
     * POST  /service-types : Create a new serviceTypes.
     *
     * @param serviceTypes the serviceTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceTypes, or with status 400 (Bad Request) if the serviceTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<ServiceTypes> createServiceTypes(@Valid @RequestBody ServiceTypes serviceTypes) throws URISyntaxException {
        log.debug("REST request to save ServiceTypes : {}", serviceTypes);
        if (serviceTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new serviceTypes cannot already have an ID")).body(null);
        }
        ServiceTypes result = serviceTypesRepository.save(serviceTypes);
        return ResponseEntity.created(new URI("/api/service-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /service-types : Updates an existing serviceTypes.
     *
     * @param serviceTypes the serviceTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceTypes,
     * or with status 400 (Bad Request) if the serviceTypes is not valid,
     * or with status 500 (Internal Server Error) if the serviceTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/service-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<ServiceTypes> updateServiceTypes(@Valid @RequestBody ServiceTypes serviceTypes) throws URISyntaxException {
        log.debug("REST request to update ServiceTypes : {}", serviceTypes);
        if (serviceTypes.getId() == null) {
            return createServiceTypes(serviceTypes);
        }
        ServiceTypes result = serviceTypesRepository.save(serviceTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /service-types : get all the serviceTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of serviceTypes in body
     */
    @GetMapping("/service-types")
    @Timed
    public List<ServiceTypes> getAllServiceTypes() {
        log.debug("REST request to get all ServiceTypes");
        return serviceTypesRepository.findAll();
    }

    /**
     * GET  /service-types/:id : get the "id" serviceTypes.
     *
     * @param id the id of the serviceTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceTypes, or with status 404 (Not Found)
     */
    @GetMapping("/service-types/{id}")
    @Timed
    public ResponseEntity<ServiceTypes> getServiceTypes(@PathVariable Long id) {
        log.debug("REST request to get ServiceTypes : {}", id);
        ServiceTypes serviceTypes = serviceTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceTypes));
    }

    /**
     * DELETE  /service-types/:id : delete the "id" serviceTypes.
     *
     * @param id the id of the serviceTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/service-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteServiceTypes(@PathVariable Long id) {
        log.debug("REST request to delete ServiceTypes : {}", id);
        serviceTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
