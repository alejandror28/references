package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.ServiceTypes;
import com.cartienda.references.repository.ServiceTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ServiceTypesResource REST controller.
 *
 * @see ServiceTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class ServiceTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ServiceTypesRepository serviceTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restServiceTypesMockMvc;

    private ServiceTypes serviceTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ServiceTypesResource serviceTypesResource = new ServiceTypesResource(serviceTypesRepository);
        this.restServiceTypesMockMvc = MockMvcBuilders.standaloneSetup(serviceTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceTypes createEntity(EntityManager em) {
        ServiceTypes serviceTypes = new ServiceTypes()
            .description(DEFAULT_DESCRIPTION);
        return serviceTypes;
    }

    @Before
    public void initTest() {
        serviceTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createServiceTypes() throws Exception {
        int databaseSizeBeforeCreate = serviceTypesRepository.findAll().size();

        // Create the ServiceTypes
        restServiceTypesMockMvc.perform(post("/api/service-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceTypes)))
            .andExpect(status().isCreated());

        // Validate the ServiceTypes in the database
        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceTypes testServiceTypes = serviceTypesList.get(serviceTypesList.size() - 1);
        assertThat(testServiceTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createServiceTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = serviceTypesRepository.findAll().size();

        // Create the ServiceTypes with an existing ID
        serviceTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceTypesMockMvc.perform(post("/api/service-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypesRepository.findAll().size();
        // set the field null
        serviceTypes.setDescription(null);

        // Create the ServiceTypes, which fails.

        restServiceTypesMockMvc.perform(post("/api/service-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceTypes)))
            .andExpect(status().isBadRequest());

        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllServiceTypes() throws Exception {
        // Initialize the database
        serviceTypesRepository.saveAndFlush(serviceTypes);

        // Get all the serviceTypesList
        restServiceTypesMockMvc.perform(get("/api/service-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getServiceTypes() throws Exception {
        // Initialize the database
        serviceTypesRepository.saveAndFlush(serviceTypes);

        // Get the serviceTypes
        restServiceTypesMockMvc.perform(get("/api/service-types/{id}", serviceTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(serviceTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingServiceTypes() throws Exception {
        // Get the serviceTypes
        restServiceTypesMockMvc.perform(get("/api/service-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateServiceTypes() throws Exception {
        // Initialize the database
        serviceTypesRepository.saveAndFlush(serviceTypes);
        int databaseSizeBeforeUpdate = serviceTypesRepository.findAll().size();

        // Update the serviceTypes
        ServiceTypes updatedServiceTypes = serviceTypesRepository.findOne(serviceTypes.getId());
        updatedServiceTypes
            .description(UPDATED_DESCRIPTION);

        restServiceTypesMockMvc.perform(put("/api/service-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedServiceTypes)))
            .andExpect(status().isOk());

        // Validate the ServiceTypes in the database
        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeUpdate);
        ServiceTypes testServiceTypes = serviceTypesList.get(serviceTypesList.size() - 1);
        assertThat(testServiceTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingServiceTypes() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypesRepository.findAll().size();

        // Create the ServiceTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restServiceTypesMockMvc.perform(put("/api/service-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceTypes)))
            .andExpect(status().isCreated());

        // Validate the ServiceTypes in the database
        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteServiceTypes() throws Exception {
        // Initialize the database
        serviceTypesRepository.saveAndFlush(serviceTypes);
        int databaseSizeBeforeDelete = serviceTypesRepository.findAll().size();

        // Get the serviceTypes
        restServiceTypesMockMvc.perform(delete("/api/service-types/{id}", serviceTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ServiceTypes> serviceTypesList = serviceTypesRepository.findAll();
        assertThat(serviceTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceTypes.class);
        ServiceTypes serviceTypes1 = new ServiceTypes();
        serviceTypes1.setId(1L);
        ServiceTypes serviceTypes2 = new ServiceTypes();
        serviceTypes2.setId(serviceTypes1.getId());
        assertThat(serviceTypes1).isEqualTo(serviceTypes2);
        serviceTypes2.setId(2L);
        assertThat(serviceTypes1).isNotEqualTo(serviceTypes2);
        serviceTypes1.setId(null);
        assertThat(serviceTypes1).isNotEqualTo(serviceTypes2);
    }
}
