package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.BreakAssistanceTypes;

import com.cartienda.references.repository.BreakAssistanceTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BreakAssistanceTypes.
 */
@RestController
@RequestMapping("/api")
public class BreakAssistanceTypesResource {

    private final Logger log = LoggerFactory.getLogger(BreakAssistanceTypesResource.class);

    private static final String ENTITY_NAME = "breakAssistanceTypes";

    private final BreakAssistanceTypesRepository breakAssistanceTypesRepository;

    public BreakAssistanceTypesResource(BreakAssistanceTypesRepository breakAssistanceTypesRepository) {
        this.breakAssistanceTypesRepository = breakAssistanceTypesRepository;
    }

    /**
     * POST  /break-assistance-types : Create a new breakAssistanceTypes.
     *
     * @param breakAssistanceTypes the breakAssistanceTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new breakAssistanceTypes, or with status 400 (Bad Request) if the breakAssistanceTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/break-assistance-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<BreakAssistanceTypes> createBreakAssistanceTypes(@Valid @RequestBody BreakAssistanceTypes breakAssistanceTypes) throws URISyntaxException {
        log.debug("REST request to save BreakAssistanceTypes : {}", breakAssistanceTypes);
        if (breakAssistanceTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new breakAssistanceTypes cannot already have an ID")).body(null);
        }
        BreakAssistanceTypes result = breakAssistanceTypesRepository.save(breakAssistanceTypes);
        return ResponseEntity.created(new URI("/api/break-assistance-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /break-assistance-types : Updates an existing breakAssistanceTypes.
     *
     * @param breakAssistanceTypes the breakAssistanceTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated breakAssistanceTypes,
     * or with status 400 (Bad Request) if the breakAssistanceTypes is not valid,
     * or with status 500 (Internal Server Error) if the breakAssistanceTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/break-assistance-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<BreakAssistanceTypes> updateBreakAssistanceTypes(@Valid @RequestBody BreakAssistanceTypes breakAssistanceTypes) throws URISyntaxException {
        log.debug("REST request to update BreakAssistanceTypes : {}", breakAssistanceTypes);
        if (breakAssistanceTypes.getId() == null) {
            return createBreakAssistanceTypes(breakAssistanceTypes);
        }
        BreakAssistanceTypes result = breakAssistanceTypesRepository.save(breakAssistanceTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, breakAssistanceTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /break-assistance-types : get all the breakAssistanceTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of breakAssistanceTypes in body
     */
    @GetMapping("/break-assistance-types")
    @Timed
    public List<BreakAssistanceTypes> getAllBreakAssistanceTypes() {
        log.debug("REST request to get all BreakAssistanceTypes");
        return breakAssistanceTypesRepository.findAll();
    }

    /**
     * GET  /break-assistance-types/:id : get the "id" breakAssistanceTypes.
     *
     * @param id the id of the breakAssistanceTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the breakAssistanceTypes, or with status 404 (Not Found)
     */
    @GetMapping("/break-assistance-types/{id}")
    @Timed
    public ResponseEntity<BreakAssistanceTypes> getBreakAssistanceTypes(@PathVariable Long id) {
        log.debug("REST request to get BreakAssistanceTypes : {}", id);
        BreakAssistanceTypes breakAssistanceTypes = breakAssistanceTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(breakAssistanceTypes));
    }

    /**
     * DELETE  /break-assistance-types/:id : delete the "id" breakAssistanceTypes.
     *
     * @param id the id of the breakAssistanceTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/break-assistance-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteBreakAssistanceTypes(@PathVariable Long id) {
        log.debug("REST request to delete BreakAssistanceTypes : {}", id);
        breakAssistanceTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
