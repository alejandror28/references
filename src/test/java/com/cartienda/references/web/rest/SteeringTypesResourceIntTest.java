package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.SteeringTypes;
import com.cartienda.references.repository.SteeringTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SteeringTypesResource REST controller.
 *
 * @see SteeringTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class SteeringTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private SteeringTypesRepository steeringTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSteeringTypesMockMvc;

    private SteeringTypes steeringTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SteeringTypesResource steeringTypesResource = new SteeringTypesResource(steeringTypesRepository);
        this.restSteeringTypesMockMvc = MockMvcBuilders.standaloneSetup(steeringTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SteeringTypes createEntity(EntityManager em) {
        SteeringTypes steeringTypes = new SteeringTypes()
            .description(DEFAULT_DESCRIPTION);
        return steeringTypes;
    }

    @Before
    public void initTest() {
        steeringTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createSteeringTypes() throws Exception {
        int databaseSizeBeforeCreate = steeringTypesRepository.findAll().size();

        // Create the SteeringTypes
        restSteeringTypesMockMvc.perform(post("/api/steering-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(steeringTypes)))
            .andExpect(status().isCreated());

        // Validate the SteeringTypes in the database
        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeCreate + 1);
        SteeringTypes testSteeringTypes = steeringTypesList.get(steeringTypesList.size() - 1);
        assertThat(testSteeringTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createSteeringTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = steeringTypesRepository.findAll().size();

        // Create the SteeringTypes with an existing ID
        steeringTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSteeringTypesMockMvc.perform(post("/api/steering-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(steeringTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = steeringTypesRepository.findAll().size();
        // set the field null
        steeringTypes.setDescription(null);

        // Create the SteeringTypes, which fails.

        restSteeringTypesMockMvc.perform(post("/api/steering-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(steeringTypes)))
            .andExpect(status().isBadRequest());

        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSteeringTypes() throws Exception {
        // Initialize the database
        steeringTypesRepository.saveAndFlush(steeringTypes);

        // Get all the steeringTypesList
        restSteeringTypesMockMvc.perform(get("/api/steering-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(steeringTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getSteeringTypes() throws Exception {
        // Initialize the database
        steeringTypesRepository.saveAndFlush(steeringTypes);

        // Get the steeringTypes
        restSteeringTypesMockMvc.perform(get("/api/steering-types/{id}", steeringTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(steeringTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSteeringTypes() throws Exception {
        // Get the steeringTypes
        restSteeringTypesMockMvc.perform(get("/api/steering-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSteeringTypes() throws Exception {
        // Initialize the database
        steeringTypesRepository.saveAndFlush(steeringTypes);
        int databaseSizeBeforeUpdate = steeringTypesRepository.findAll().size();

        // Update the steeringTypes
        SteeringTypes updatedSteeringTypes = steeringTypesRepository.findOne(steeringTypes.getId());
        updatedSteeringTypes
            .description(UPDATED_DESCRIPTION);

        restSteeringTypesMockMvc.perform(put("/api/steering-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSteeringTypes)))
            .andExpect(status().isOk());

        // Validate the SteeringTypes in the database
        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeUpdate);
        SteeringTypes testSteeringTypes = steeringTypesList.get(steeringTypesList.size() - 1);
        assertThat(testSteeringTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingSteeringTypes() throws Exception {
        int databaseSizeBeforeUpdate = steeringTypesRepository.findAll().size();

        // Create the SteeringTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSteeringTypesMockMvc.perform(put("/api/steering-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(steeringTypes)))
            .andExpect(status().isCreated());

        // Validate the SteeringTypes in the database
        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSteeringTypes() throws Exception {
        // Initialize the database
        steeringTypesRepository.saveAndFlush(steeringTypes);
        int databaseSizeBeforeDelete = steeringTypesRepository.findAll().size();

        // Get the steeringTypes
        restSteeringTypesMockMvc.perform(delete("/api/steering-types/{id}", steeringTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SteeringTypes> steeringTypesList = steeringTypesRepository.findAll();
        assertThat(steeringTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SteeringTypes.class);
        SteeringTypes steeringTypes1 = new SteeringTypes();
        steeringTypes1.setId(1L);
        SteeringTypes steeringTypes2 = new SteeringTypes();
        steeringTypes2.setId(steeringTypes1.getId());
        assertThat(steeringTypes1).isEqualTo(steeringTypes2);
        steeringTypes2.setId(2L);
        assertThat(steeringTypes1).isNotEqualTo(steeringTypes2);
        steeringTypes1.setId(null);
        assertThat(steeringTypes1).isNotEqualTo(steeringTypes2);
    }
}
