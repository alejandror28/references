package com.cartienda.references.repository;

import com.cartienda.references.domain.UpholsteryTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UpholsteryTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UpholsteryTypesRepository extends JpaRepository<UpholsteryTypes,Long> {
    
}
