package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.AirConditionicTypes;
import com.cartienda.references.repository.AirConditionicTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AirConditionicTypesResource REST controller.
 *
 * @see AirConditionicTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class AirConditionicTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AirConditionicTypesRepository airConditionicTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAirConditionicTypesMockMvc;

    private AirConditionicTypes airConditionicTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AirConditionicTypesResource airConditionicTypesResource = new AirConditionicTypesResource(airConditionicTypesRepository);
        this.restAirConditionicTypesMockMvc = MockMvcBuilders.standaloneSetup(airConditionicTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AirConditionicTypes createEntity(EntityManager em) {
        AirConditionicTypes airConditionicTypes = new AirConditionicTypes()
            .description(DEFAULT_DESCRIPTION);
        return airConditionicTypes;
    }

    @Before
    public void initTest() {
        airConditionicTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createAirConditionicTypes() throws Exception {
        int databaseSizeBeforeCreate = airConditionicTypesRepository.findAll().size();

        // Create the AirConditionicTypes
        restAirConditionicTypesMockMvc.perform(post("/api/air-conditionic-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionicTypes)))
            .andExpect(status().isCreated());

        // Validate the AirConditionicTypes in the database
        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeCreate + 1);
        AirConditionicTypes testAirConditionicTypes = airConditionicTypesList.get(airConditionicTypesList.size() - 1);
        assertThat(testAirConditionicTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createAirConditionicTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = airConditionicTypesRepository.findAll().size();

        // Create the AirConditionicTypes with an existing ID
        airConditionicTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAirConditionicTypesMockMvc.perform(post("/api/air-conditionic-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionicTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = airConditionicTypesRepository.findAll().size();
        // set the field null
        airConditionicTypes.setDescription(null);

        // Create the AirConditionicTypes, which fails.

        restAirConditionicTypesMockMvc.perform(post("/api/air-conditionic-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionicTypes)))
            .andExpect(status().isBadRequest());

        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAirConditionicTypes() throws Exception {
        // Initialize the database
        airConditionicTypesRepository.saveAndFlush(airConditionicTypes);

        // Get all the airConditionicTypesList
        restAirConditionicTypesMockMvc.perform(get("/api/air-conditionic-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(airConditionicTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getAirConditionicTypes() throws Exception {
        // Initialize the database
        airConditionicTypesRepository.saveAndFlush(airConditionicTypes);

        // Get the airConditionicTypes
        restAirConditionicTypesMockMvc.perform(get("/api/air-conditionic-types/{id}", airConditionicTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(airConditionicTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAirConditionicTypes() throws Exception {
        // Get the airConditionicTypes
        restAirConditionicTypesMockMvc.perform(get("/api/air-conditionic-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAirConditionicTypes() throws Exception {
        // Initialize the database
        airConditionicTypesRepository.saveAndFlush(airConditionicTypes);
        int databaseSizeBeforeUpdate = airConditionicTypesRepository.findAll().size();

        // Update the airConditionicTypes
        AirConditionicTypes updatedAirConditionicTypes = airConditionicTypesRepository.findOne(airConditionicTypes.getId());
        updatedAirConditionicTypes
            .description(UPDATED_DESCRIPTION);

        restAirConditionicTypesMockMvc.perform(put("/api/air-conditionic-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAirConditionicTypes)))
            .andExpect(status().isOk());

        // Validate the AirConditionicTypes in the database
        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeUpdate);
        AirConditionicTypes testAirConditionicTypes = airConditionicTypesList.get(airConditionicTypesList.size() - 1);
        assertThat(testAirConditionicTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingAirConditionicTypes() throws Exception {
        int databaseSizeBeforeUpdate = airConditionicTypesRepository.findAll().size();

        // Create the AirConditionicTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAirConditionicTypesMockMvc.perform(put("/api/air-conditionic-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(airConditionicTypes)))
            .andExpect(status().isCreated());

        // Validate the AirConditionicTypes in the database
        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAirConditionicTypes() throws Exception {
        // Initialize the database
        airConditionicTypesRepository.saveAndFlush(airConditionicTypes);
        int databaseSizeBeforeDelete = airConditionicTypesRepository.findAll().size();

        // Get the airConditionicTypes
        restAirConditionicTypesMockMvc.perform(delete("/api/air-conditionic-types/{id}", airConditionicTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AirConditionicTypes> airConditionicTypesList = airConditionicTypesRepository.findAll();
        assertThat(airConditionicTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AirConditionicTypes.class);
        AirConditionicTypes airConditionicTypes1 = new AirConditionicTypes();
        airConditionicTypes1.setId(1L);
        AirConditionicTypes airConditionicTypes2 = new AirConditionicTypes();
        airConditionicTypes2.setId(airConditionicTypes1.getId());
        assertThat(airConditionicTypes1).isEqualTo(airConditionicTypes2);
        airConditionicTypes2.setId(2L);
        assertThat(airConditionicTypes1).isNotEqualTo(airConditionicTypes2);
        airConditionicTypes1.setId(null);
        assertThat(airConditionicTypes1).isNotEqualTo(airConditionicTypes2);
    }
}
