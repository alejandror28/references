package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.VehicleStatus;
import com.cartienda.references.repository.VehicleStatusRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VehicleStatusResource REST controller.
 *
 * @see VehicleStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class VehicleStatusResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private VehicleStatusRepository vehicleStatusRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVehicleStatusMockMvc;

    private VehicleStatus vehicleStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VehicleStatusResource vehicleStatusResource = new VehicleStatusResource(vehicleStatusRepository);
        this.restVehicleStatusMockMvc = MockMvcBuilders.standaloneSetup(vehicleStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VehicleStatus createEntity(EntityManager em) {
        VehicleStatus vehicleStatus = new VehicleStatus()
            .description(DEFAULT_DESCRIPTION);
        return vehicleStatus;
    }

    @Before
    public void initTest() {
        vehicleStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicleStatus() throws Exception {
        int databaseSizeBeforeCreate = vehicleStatusRepository.findAll().size();

        // Create the VehicleStatus
        restVehicleStatusMockMvc.perform(post("/api/vehicle-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicleStatus)))
            .andExpect(status().isCreated());

        // Validate the VehicleStatus in the database
        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeCreate + 1);
        VehicleStatus testVehicleStatus = vehicleStatusList.get(vehicleStatusList.size() - 1);
        assertThat(testVehicleStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createVehicleStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vehicleStatusRepository.findAll().size();

        // Create the VehicleStatus with an existing ID
        vehicleStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVehicleStatusMockMvc.perform(post("/api/vehicle-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicleStatus)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleStatusRepository.findAll().size();
        // set the field null
        vehicleStatus.setDescription(null);

        // Create the VehicleStatus, which fails.

        restVehicleStatusMockMvc.perform(post("/api/vehicle-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicleStatus)))
            .andExpect(status().isBadRequest());

        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVehicleStatuses() throws Exception {
        // Initialize the database
        vehicleStatusRepository.saveAndFlush(vehicleStatus);

        // Get all the vehicleStatusList
        restVehicleStatusMockMvc.perform(get("/api/vehicle-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicleStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getVehicleStatus() throws Exception {
        // Initialize the database
        vehicleStatusRepository.saveAndFlush(vehicleStatus);

        // Get the vehicleStatus
        restVehicleStatusMockMvc.perform(get("/api/vehicle-statuses/{id}", vehicleStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vehicleStatus.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVehicleStatus() throws Exception {
        // Get the vehicleStatus
        restVehicleStatusMockMvc.perform(get("/api/vehicle-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicleStatus() throws Exception {
        // Initialize the database
        vehicleStatusRepository.saveAndFlush(vehicleStatus);
        int databaseSizeBeforeUpdate = vehicleStatusRepository.findAll().size();

        // Update the vehicleStatus
        VehicleStatus updatedVehicleStatus = vehicleStatusRepository.findOne(vehicleStatus.getId());
        updatedVehicleStatus
            .description(UPDATED_DESCRIPTION);

        restVehicleStatusMockMvc.perform(put("/api/vehicle-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVehicleStatus)))
            .andExpect(status().isOk());

        // Validate the VehicleStatus in the database
        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeUpdate);
        VehicleStatus testVehicleStatus = vehicleStatusList.get(vehicleStatusList.size() - 1);
        assertThat(testVehicleStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingVehicleStatus() throws Exception {
        int databaseSizeBeforeUpdate = vehicleStatusRepository.findAll().size();

        // Create the VehicleStatus

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVehicleStatusMockMvc.perform(put("/api/vehicle-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicleStatus)))
            .andExpect(status().isCreated());

        // Validate the VehicleStatus in the database
        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVehicleStatus() throws Exception {
        // Initialize the database
        vehicleStatusRepository.saveAndFlush(vehicleStatus);
        int databaseSizeBeforeDelete = vehicleStatusRepository.findAll().size();

        // Get the vehicleStatus
        restVehicleStatusMockMvc.perform(delete("/api/vehicle-statuses/{id}", vehicleStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<VehicleStatus> vehicleStatusList = vehicleStatusRepository.findAll();
        assertThat(vehicleStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VehicleStatus.class);
        VehicleStatus vehicleStatus1 = new VehicleStatus();
        vehicleStatus1.setId(1L);
        VehicleStatus vehicleStatus2 = new VehicleStatus();
        vehicleStatus2.setId(vehicleStatus1.getId());
        assertThat(vehicleStatus1).isEqualTo(vehicleStatus2);
        vehicleStatus2.setId(2L);
        assertThat(vehicleStatus1).isNotEqualTo(vehicleStatus2);
        vehicleStatus1.setId(null);
        assertThat(vehicleStatus1).isNotEqualTo(vehicleStatus2);
    }
}
