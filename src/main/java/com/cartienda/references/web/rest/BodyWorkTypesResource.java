package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.BodyWorkTypes;

import com.cartienda.references.repository.BodyWorkTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BodyWorkTypes.
 */
@RestController
@RequestMapping("/api")
public class BodyWorkTypesResource {

    private final Logger log = LoggerFactory.getLogger(BodyWorkTypesResource.class);

    private static final String ENTITY_NAME = "bodyWorkTypes";

    private final BodyWorkTypesRepository bodyWorkTypesRepository;

    public BodyWorkTypesResource(BodyWorkTypesRepository bodyWorkTypesRepository) {
        this.bodyWorkTypesRepository = bodyWorkTypesRepository;
    }

    /**
     * POST  /body-work-types : Create a new bodyWorkTypes.
     *
     * @param bodyWorkTypes the bodyWorkTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bodyWorkTypes, or with status 400 (Bad Request) if the bodyWorkTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/body-work-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<BodyWorkTypes> createBodyWorkTypes(@Valid @RequestBody BodyWorkTypes bodyWorkTypes) throws URISyntaxException {
        log.debug("REST request to save BodyWorkTypes : {}", bodyWorkTypes);
        if (bodyWorkTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new bodyWorkTypes cannot already have an ID")).body(null);
        }
        BodyWorkTypes result = bodyWorkTypesRepository.save(bodyWorkTypes);
        return ResponseEntity.created(new URI("/api/body-work-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /body-work-types : Updates an existing bodyWorkTypes.
     *
     * @param bodyWorkTypes the bodyWorkTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bodyWorkTypes,
     * or with status 400 (Bad Request) if the bodyWorkTypes is not valid,
     * or with status 500 (Internal Server Error) if the bodyWorkTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/body-work-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<BodyWorkTypes> updateBodyWorkTypes(@Valid @RequestBody BodyWorkTypes bodyWorkTypes) throws URISyntaxException {
        log.debug("REST request to update BodyWorkTypes : {}", bodyWorkTypes);
        if (bodyWorkTypes.getId() == null) {
            return createBodyWorkTypes(bodyWorkTypes);
        }
        BodyWorkTypes result = bodyWorkTypesRepository.save(bodyWorkTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bodyWorkTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /body-work-types : get all the bodyWorkTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of bodyWorkTypes in body
     */
    @GetMapping("/body-work-types")
    @Timed
    public List<BodyWorkTypes> getAllBodyWorkTypes() {
        log.debug("REST request to get all BodyWorkTypes");
        return bodyWorkTypesRepository.findAll();
    }

    /**
     * GET  /body-work-types/:id : get the "id" bodyWorkTypes.
     *
     * @param id the id of the bodyWorkTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bodyWorkTypes, or with status 404 (Not Found)
     */
    @GetMapping("/body-work-types/{id}")
    @Timed
    public ResponseEntity<BodyWorkTypes> getBodyWorkTypes(@PathVariable Long id) {
        log.debug("REST request to get BodyWorkTypes : {}", id);
        BodyWorkTypes bodyWorkTypes = bodyWorkTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bodyWorkTypes));
    }

    /**
     * DELETE  /body-work-types/:id : delete the "id" bodyWorkTypes.
     *
     * @param id the id of the bodyWorkTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/body-work-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteBodyWorkTypes(@PathVariable Long id) {
        log.debug("REST request to delete BodyWorkTypes : {}", id);
        bodyWorkTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
