package com.cartienda.references.repository;

import com.cartienda.references.domain.AirbagTypes;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AirbagTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AirbagTypesRepository extends JpaRepository<AirbagTypes,Long> {
    
}
