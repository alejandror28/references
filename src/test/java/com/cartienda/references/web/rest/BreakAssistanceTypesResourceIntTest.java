package com.cartienda.references.web.rest;

import com.cartienda.references.ReferencesApp;

import com.cartienda.references.domain.BreakAssistanceTypes;
import com.cartienda.references.repository.BreakAssistanceTypesRepository;
import com.cartienda.references.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BreakAssistanceTypesResource REST controller.
 *
 * @see BreakAssistanceTypesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReferencesApp.class)
public class BreakAssistanceTypesResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private BreakAssistanceTypesRepository breakAssistanceTypesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBreakAssistanceTypesMockMvc;

    private BreakAssistanceTypes breakAssistanceTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BreakAssistanceTypesResource breakAssistanceTypesResource = new BreakAssistanceTypesResource(breakAssistanceTypesRepository);
        this.restBreakAssistanceTypesMockMvc = MockMvcBuilders.standaloneSetup(breakAssistanceTypesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BreakAssistanceTypes createEntity(EntityManager em) {
        BreakAssistanceTypes breakAssistanceTypes = new BreakAssistanceTypes()
            .description(DEFAULT_DESCRIPTION);
        return breakAssistanceTypes;
    }

    @Before
    public void initTest() {
        breakAssistanceTypes = createEntity(em);
    }

    @Test
    @Transactional
    public void createBreakAssistanceTypes() throws Exception {
        int databaseSizeBeforeCreate = breakAssistanceTypesRepository.findAll().size();

        // Create the BreakAssistanceTypes
        restBreakAssistanceTypesMockMvc.perform(post("/api/break-assistance-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(breakAssistanceTypes)))
            .andExpect(status().isCreated());

        // Validate the BreakAssistanceTypes in the database
        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeCreate + 1);
        BreakAssistanceTypes testBreakAssistanceTypes = breakAssistanceTypesList.get(breakAssistanceTypesList.size() - 1);
        assertThat(testBreakAssistanceTypes.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createBreakAssistanceTypesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = breakAssistanceTypesRepository.findAll().size();

        // Create the BreakAssistanceTypes with an existing ID
        breakAssistanceTypes.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBreakAssistanceTypesMockMvc.perform(post("/api/break-assistance-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(breakAssistanceTypes)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = breakAssistanceTypesRepository.findAll().size();
        // set the field null
        breakAssistanceTypes.setDescription(null);

        // Create the BreakAssistanceTypes, which fails.

        restBreakAssistanceTypesMockMvc.perform(post("/api/break-assistance-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(breakAssistanceTypes)))
            .andExpect(status().isBadRequest());

        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBreakAssistanceTypes() throws Exception {
        // Initialize the database
        breakAssistanceTypesRepository.saveAndFlush(breakAssistanceTypes);

        // Get all the breakAssistanceTypesList
        restBreakAssistanceTypesMockMvc.perform(get("/api/break-assistance-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(breakAssistanceTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getBreakAssistanceTypes() throws Exception {
        // Initialize the database
        breakAssistanceTypesRepository.saveAndFlush(breakAssistanceTypes);

        // Get the breakAssistanceTypes
        restBreakAssistanceTypesMockMvc.perform(get("/api/break-assistance-types/{id}", breakAssistanceTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(breakAssistanceTypes.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBreakAssistanceTypes() throws Exception {
        // Get the breakAssistanceTypes
        restBreakAssistanceTypesMockMvc.perform(get("/api/break-assistance-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBreakAssistanceTypes() throws Exception {
        // Initialize the database
        breakAssistanceTypesRepository.saveAndFlush(breakAssistanceTypes);
        int databaseSizeBeforeUpdate = breakAssistanceTypesRepository.findAll().size();

        // Update the breakAssistanceTypes
        BreakAssistanceTypes updatedBreakAssistanceTypes = breakAssistanceTypesRepository.findOne(breakAssistanceTypes.getId());
        updatedBreakAssistanceTypes
            .description(UPDATED_DESCRIPTION);

        restBreakAssistanceTypesMockMvc.perform(put("/api/break-assistance-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBreakAssistanceTypes)))
            .andExpect(status().isOk());

        // Validate the BreakAssistanceTypes in the database
        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeUpdate);
        BreakAssistanceTypes testBreakAssistanceTypes = breakAssistanceTypesList.get(breakAssistanceTypesList.size() - 1);
        assertThat(testBreakAssistanceTypes.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingBreakAssistanceTypes() throws Exception {
        int databaseSizeBeforeUpdate = breakAssistanceTypesRepository.findAll().size();

        // Create the BreakAssistanceTypes

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBreakAssistanceTypesMockMvc.perform(put("/api/break-assistance-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(breakAssistanceTypes)))
            .andExpect(status().isCreated());

        // Validate the BreakAssistanceTypes in the database
        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBreakAssistanceTypes() throws Exception {
        // Initialize the database
        breakAssistanceTypesRepository.saveAndFlush(breakAssistanceTypes);
        int databaseSizeBeforeDelete = breakAssistanceTypesRepository.findAll().size();

        // Get the breakAssistanceTypes
        restBreakAssistanceTypesMockMvc.perform(delete("/api/break-assistance-types/{id}", breakAssistanceTypes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BreakAssistanceTypes> breakAssistanceTypesList = breakAssistanceTypesRepository.findAll();
        assertThat(breakAssistanceTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BreakAssistanceTypes.class);
        BreakAssistanceTypes breakAssistanceTypes1 = new BreakAssistanceTypes();
        breakAssistanceTypes1.setId(1L);
        BreakAssistanceTypes breakAssistanceTypes2 = new BreakAssistanceTypes();
        breakAssistanceTypes2.setId(breakAssistanceTypes1.getId());
        assertThat(breakAssistanceTypes1).isEqualTo(breakAssistanceTypes2);
        breakAssistanceTypes2.setId(2L);
        assertThat(breakAssistanceTypes1).isNotEqualTo(breakAssistanceTypes2);
        breakAssistanceTypes1.setId(null);
        assertThat(breakAssistanceTypes1).isNotEqualTo(breakAssistanceTypes2);
    }
}
