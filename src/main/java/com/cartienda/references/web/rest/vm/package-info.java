/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cartienda.references.web.rest.vm;
