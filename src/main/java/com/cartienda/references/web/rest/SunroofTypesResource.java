package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.SunroofTypes;

import com.cartienda.references.repository.SunroofTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SunroofTypes.
 */
@RestController
@RequestMapping("/api")
public class SunroofTypesResource {

    private final Logger log = LoggerFactory.getLogger(SunroofTypesResource.class);

    private static final String ENTITY_NAME = "sunroofTypes";

    private final SunroofTypesRepository sunroofTypesRepository;

    public SunroofTypesResource(SunroofTypesRepository sunroofTypesRepository) {
        this.sunroofTypesRepository = sunroofTypesRepository;
    }

    /**
     * POST  /sunroof-types : Create a new sunroofTypes.
     *
     * @param sunroofTypes the sunroofTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sunroofTypes, or with status 400 (Bad Request) if the sunroofTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sunroof-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<SunroofTypes> createSunroofTypes(@Valid @RequestBody SunroofTypes sunroofTypes) throws URISyntaxException {
        log.debug("REST request to save SunroofTypes : {}", sunroofTypes);
        if (sunroofTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new sunroofTypes cannot already have an ID")).body(null);
        }
        SunroofTypes result = sunroofTypesRepository.save(sunroofTypes);
        return ResponseEntity.created(new URI("/api/sunroof-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sunroof-types : Updates an existing sunroofTypes.
     *
     * @param sunroofTypes the sunroofTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sunroofTypes,
     * or with status 400 (Bad Request) if the sunroofTypes is not valid,
     * or with status 500 (Internal Server Error) if the sunroofTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sunroof-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<SunroofTypes> updateSunroofTypes(@Valid @RequestBody SunroofTypes sunroofTypes) throws URISyntaxException {
        log.debug("REST request to update SunroofTypes : {}", sunroofTypes);
        if (sunroofTypes.getId() == null) {
            return createSunroofTypes(sunroofTypes);
        }
        SunroofTypes result = sunroofTypesRepository.save(sunroofTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sunroofTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sunroof-types : get all the sunroofTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sunroofTypes in body
     */
    @GetMapping("/sunroof-types")
    @Timed
    public List<SunroofTypes> getAllSunroofTypes() {
        log.debug("REST request to get all SunroofTypes");
        return sunroofTypesRepository.findAll();
    }

    /**
     * GET  /sunroof-types/:id : get the "id" sunroofTypes.
     *
     * @param id the id of the sunroofTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sunroofTypes, or with status 404 (Not Found)
     */
    @GetMapping("/sunroof-types/{id}")
    @Timed
    public ResponseEntity<SunroofTypes> getSunroofTypes(@PathVariable Long id) {
        log.debug("REST request to get SunroofTypes : {}", id);
        SunroofTypes sunroofTypes = sunroofTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sunroofTypes));
    }

    /**
     * DELETE  /sunroof-types/:id : delete the "id" sunroofTypes.
     *
     * @param id the id of the sunroofTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sunroof-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteSunroofTypes(@PathVariable Long id) {
        log.debug("REST request to delete SunroofTypes : {}", id);
        sunroofTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
