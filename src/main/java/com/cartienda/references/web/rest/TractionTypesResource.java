package com.cartienda.references.web.rest;

import com.cartienda.references.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cartienda.references.domain.TractionTypes;

import com.cartienda.references.repository.TractionTypesRepository;
import com.cartienda.references.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TractionTypes.
 */
@RestController
@RequestMapping("/api")
public class TractionTypesResource {

    private final Logger log = LoggerFactory.getLogger(TractionTypesResource.class);

    private static final String ENTITY_NAME = "tractionTypes";

    private final TractionTypesRepository tractionTypesRepository;

    public TractionTypesResource(TractionTypesRepository tractionTypesRepository) {
        this.tractionTypesRepository = tractionTypesRepository;
    }

    /**
     * POST  /traction-types : Create a new tractionTypes.
     *
     * @param tractionTypes the tractionTypes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tractionTypes, or with status 400 (Bad Request) if the tractionTypes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/traction-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TractionTypes> createTractionTypes(@Valid @RequestBody TractionTypes tractionTypes) throws URISyntaxException {
        log.debug("REST request to save TractionTypes : {}", tractionTypes);
        if (tractionTypes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tractionTypes cannot already have an ID")).body(null);
        }
        TractionTypes result = tractionTypesRepository.save(tractionTypes);
        return ResponseEntity.created(new URI("/api/traction-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /traction-types : Updates an existing tractionTypes.
     *
     * @param tractionTypes the tractionTypes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tractionTypes,
     * or with status 400 (Bad Request) if the tractionTypes is not valid,
     * or with status 500 (Internal Server Error) if the tractionTypes couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/traction-types")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<TractionTypes> updateTractionTypes(@Valid @RequestBody TractionTypes tractionTypes) throws URISyntaxException {
        log.debug("REST request to update TractionTypes : {}", tractionTypes);
        if (tractionTypes.getId() == null) {
            return createTractionTypes(tractionTypes);
        }
        TractionTypes result = tractionTypesRepository.save(tractionTypes);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tractionTypes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /traction-types : get all the tractionTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tractionTypes in body
     */
    @GetMapping("/traction-types")
    @Timed
    public List<TractionTypes> getAllTractionTypes() {
        log.debug("REST request to get all TractionTypes");
        return tractionTypesRepository.findAll();
    }

    /**
     * GET  /traction-types/:id : get the "id" tractionTypes.
     *
     * @param id the id of the tractionTypes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tractionTypes, or with status 404 (Not Found)
     */
    @GetMapping("/traction-types/{id}")
    @Timed
    public ResponseEntity<TractionTypes> getTractionTypes(@PathVariable Long id) {
        log.debug("REST request to get TractionTypes : {}", id);
        TractionTypes tractionTypes = tractionTypesRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tractionTypes));
    }

    /**
     * DELETE  /traction-types/:id : delete the "id" tractionTypes.
     *
     * @param id the id of the tractionTypes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traction-types/{id}")
    @Secured(AuthoritiesConstants.ADMIN)
    @Timed
    public ResponseEntity<Void> deleteTractionTypes(@PathVariable Long id) {
        log.debug("REST request to delete TractionTypes : {}", id);
        tractionTypesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
